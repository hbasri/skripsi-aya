<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GuruAsisten;
use App\Http\Requests\GuruAsistenRequest; 

class GuruAsistenController extends Controller
{
    //
    //
    public function index(Request $request){
        $data['title'] = "Guru Asisten";
        $data['guruasisten'] = GuruAsisten::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $nama = "";
            $jenis_kelamin = "";
            $kode_staff = "";
            $email = "";
            $tipe_staff = "";
            $pendidikan_terakhir = "";
            $instansi_pendidikan_terakhir = "";

            if (isset($_GET['nama'])) {
                if (!empty($_GET['nama'])) {
                    $nama = trim($_GET['nama']);
                }
            }

           	if (isset($_GET['jenis_kelamin'])) {
                if (!empty($_GET['jenis_kelamin'])) {
                    $jenis_kelamin = trim($_GET['jenis_kelamin']);
                }
            }

            if (isset($_GET['kode_staff'])) {
                if (!empty($_GET['kode_staff'])) {
                    $kode_staff = trim($_GET['kode_staff']);
                }
            }

            if (isset($_GET['email'])) {
                if (!empty($_GET['email'])) {
                    $email = trim($_GET['email']);
                }
            }

            if (isset($_GET['tipe_staff'])) {
                if (!empty($_GET['tipe_staff'])) {
                    $tipe_staff = trim($_GET['tipe_staff']);
                }
            }

            if (isset($_GET['pendidikan_terakhir'])) {
                if (!empty($_GET['pendidikan_terakhir'])) {
                    $pendidikan_terakhir = trim($_GET['pendidikan_terakhir']);
                }
            }

            if (isset($_GET['instansi_pendidikan_terakhir'])) {
                if (!empty($_GET['instansi_pendidikan_terakhir'])) {
                    $instansi_pendidikan_terakhir = trim($_GET['instansi_pendidikan_terakhir']);
                }
            }

            $guruasisten = GuruAsisten::search($nama,$jenis_kelamin,$kode_staff,$email,$tipe_staff,$pendidikan_terakhir,$instansi_pendidikan_terakhir)->paginate(10);
            $output['guruasisten'] = view("cms.guruasisten.guruasisten", ['guruasisten' => $guruasisten])->render();
            $output['ul_guruasisten'] = view("cms.guruasisten.ul", ['guruasisten' => $guruasisten])->render();

            return response()->json($output, 200);
        }

        return view("cms.guruasisten.index",$data);
    }

    public function show($id){
        // $guruasisten = GuruAsisten::find($id);
        // $output=[];
        // $output['nama'] = $daftarharga->nama;
        // $output['harga'] = $daftarharga->harga;
        // $output['deskripsi'] = $daftarharga->deskripsi;
        // $output['created_at'] = (string) $daftarharga->created_at;

        // return response()->json($output,200);
    }

    public function create(){
        $data['title'] = "Create Guru Asisten";

        return view("cms.guruasisten.create",$data);
    }

    public function store(GuruAsistenRequest $request){
        $guruasisten = new GuruAsisten();
        $guruasisten->nama = $request->input("nama");
        $guruasisten->jenis_kelamin = $request->input("jenis_kelamin");
        $guruasisten->kode_staff = $request->input("kode_staff");
        $guruasisten->email = $request->input("email");
        $guruasisten->tipe_staff = $request->input("tipe_staff");
        $guruasisten->pendidikan_terakhir = $request->input("pendidikan_terakhir");
        $guruasisten->instansi_pendidikan_terakhir = $request->input("instansi_pendidikan_terakhir");
        $guruasisten->save();

        return redirect("cms/guru-asisten")->with("success","Guru / Asisten berhasil ditambah");
    }

    public function edit($id){
        $data['title'] = "Edit Guru Asisten";
        $data['guruasisten'] = GuruAsisten::find($id);

        return view("cms.guruasisten.edit",$data);

    }

    public function update(GuruAsistenRequest $request,$id){
        $guruasisten = GuruAsisten::find($id);
        $guruasisten->nama = $request->input("nama");
        $guruasisten->jenis_kelamin = $request->input("jenis_kelamin");
        $guruasisten->kode_staff = $request->input("kode_staff");
        $guruasisten->email = $request->input("email");
        $guruasisten->tipe_staff = $request->input("tipe_staff");
        $guruasisten->pendidikan_terakhir = $request->input("pendidikan_terakhir");
        $guruasisten->instansi_pendidikan_terakhir = $request->input("instansi_pendidikan_terakhir");
        $guruasisten->save();

        return redirect("cms/guru-asisten")->with("success","Guru / Asisten berhasil diubah");
    }

    public function delete($id){
        $guruasisten = GuruAsisten::find($id)->delete();
        return redirect("cms/guru-asisten")->with("success","Guru Asisten berhasil dihapus");
    }
}
