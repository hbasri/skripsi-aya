<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StatusPembayaran;
use App\Pemesanan;
use DB;
use PDF;

class StatusPembayaranController extends Controller
{
    //
	public function store(Request $request){
		try {
			DB::beginTransaction();

			$pembayaran = StatusPembayaran::where("order_id",$request->input("order_id"))->first();
			if($pembayaran){
				$pembayaran->payment_number = $request->input("nomor_rekening");
				$pembayaran->payment_date = $request->input("tanggal_bayar");
				$pembayaran->payment_via = $request->input("bayar_via");
				$pembayaran->payment_status = "lunas";
				$pembayaran->save();

				$pemesanan = Pemesanan::find($request->input("order_id"));
				$pemesanan->order_status = "done";
				$pemesanan->save();

				DB::commit();
			}

			return response()->json(['status' => true]);
		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['status' => false, 'message' => $e->getMessage()]);
		}
		
	}

	public function index(Request $request){
        $data['title'] = "Pembayaran";
        $data['pembayaran'] = StatusPembayaran::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $nama = "";
            $tanggal = "";
            $jumlah = "";
            $status="";

            if (isset($_GET['nama'])) {
                if (!empty($_GET['nama'])) {
                    $nama = trim($_GET['nama']);
                }
            }

            if (isset($_GET['tanggal'])) {
                if (!empty($_GET['tanggal'])) {
                    $tanggal = trim($_GET['tanggal']);
                }
            }

            if (isset($_GET['jumlah'])) {
                if (!empty($_GET['jumlah'])) {
                    $jumlah = trim($_GET['jumlah']);
                }
            }

            if (isset($_GET['status'])) {
                if (!empty($_GET['status'])) {
                    $status = trim($_GET['status']);
                }
            }

            $pembayaran = StatusPembayaran::search($nama,$tanggal,$jumlah,$status)->paginate(10);
            $output['pembayaran'] = view("cms.pembayaran.pembayaran", ['pembayaran' => $pembayaran])->render();
            $output['ul_pembayaran'] = view("cms.pembayaran.ul", ['pembayaran' => $pembayaran])->render();

            return response()->json($output, 200);
        }

        return view("cms.pembayaran.index",$data);
    }

    public function report(){
        $data['title'] = "Laporan Pembayaran";
        return view("cms.pembayaran.report",$data);
    }

    public function postReport(Request $request){
        $dari = $request->input("dari");
        $sampai = $request->input("sampai");

        $data['dari'] = $dari;
        $data['sampai'] = $sampai;
        $data['pembayaran'] = StatusPembayaran::report($dari,$sampai)->get();

        $pdf = PDF::loadView('cms.pembayaran.print_report', $data)->setPaper('a4','potrait');
        return $pdf->stream('result.pdf'); 
    }
}
