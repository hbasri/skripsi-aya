<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\DaftarHarga;
use App\GuruAsisten;
use App\Http\Requests\JadwalRequest; 

class JadwalController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "Jadwal";
        $data['jadwal'] = Jadwal::orderBy("id","desc")->has("guru")->has("asisten")->paginate(10);
        $data['guru'] = GuruAsisten::where("tipe_staff","guru")->get();
        $data['asisten'] = GuruAsisten::where("tipe_staff","asisten")->get();
        $data['daftarharga'] = DaftarHarga::all();

        if ($request->ajax()) {
            $guruid = "";
            $asistenid = "";
            $waktu = "";
            $tanggal = "";
            $tempat = "";
            $daftarharga = "";

            if (isset($_GET['guruid'])) {
                if (!empty($_GET['guruid'])) {
                    $guruid = trim($_GET['guruid']);
                }
            }

            if (isset($_GET['daftarharga'])) {
                if (!empty($_GET['daftarharga'])) {
                    $daftarharga = trim($_GET['daftarharga']);
                }
            }

            if (isset($_GET['asistenid'])) {
                if (!empty($_GET['asistenid'])) {
                    $asistenid = trim($_GET['asistenid']);
                }
            }

            if (isset($_GET['waktu'])) {
                if (!empty($_GET['waktu'])) {
                    $waktu = trim($_GET['waktu']);
                }
            }

            if (isset($_GET['tanggal'])) {
                if (!empty($_GET['tanggal'])) {
                    $tanggal = trim($_GET['tanggal']);
                }
            }

            if (isset($_GET['tempat'])) {
                if (!empty($_GET['tempat'])) {
                    $tempat = trim($_GET['tempat']);
                }
            }

            $jadwal = Jadwal::search($guruid,$asistenid,$waktu,$tanggal,$tempat,$daftarharga)->paginate(10);
            $output['jadwal'] = view("cms.jadwal.jadwal", ['jadwal' => $jadwal])->render();
            $output['ul_jadwal'] = view("cms.jadwal.ul", ['jadwal' => $jadwal])->render();

            return response()->json($output, 200);
        }

        return view("cms.jadwal.index",$data);
    }

    public function show($id){
        $daftarharga = Jadwal::find($id);
        // $output=[];
        // $output['nama'] = $daftarharga->nama;
        // $output['harga'] = $daftarharga->harga;
        // $output['deskripsi'] = $daftarharga->deskripsi;
        // $output['created_at'] = (string) $daftarharga->created_at;

        return response()->json($output,200);
    }

    public function create(){
        $data['title'] = "Create Jadwal";
        $data['guru'] = GuruAsisten::where("tipe_staff","guru")->get();
        $data['asisten'] = GuruAsisten::where("tipe_staff","asisten")->get();
        $data['daftarharga'] = DaftarHarga::all();

        return view("cms.jadwal.create",$data);
    }

    public function store(JadwalRequest $request){
        $jadwal = new Jadwal();
        $jadwal->guru_id = $request->input("guru_id");
        $jadwal->asisten_id = $request->input("asisten_id");
        $jadwal->waktu = $request->input("waktu");
        $jadwal->tanggal = $request->input("tanggal");
        $jadwal->tempat = $request->input("tempat");
        $jadwal->daftar_harga_id = $request->input("daftar_harga_id");

        $jadwal->save();

        return redirect("cms/jadwal")->with("success","Jadwal berhasil ditambah");
    }

    public function edit($id){
        $data['title'] = "Edit Jadwal";
        $data['jadwal'] = Jadwal::find($id);
        $data['guru'] = GuruAsisten::where("tipe_staff","guru")->get();
        $data['asisten'] = GuruAsisten::where("tipe_staff","asisten")->get();
        $data['daftarharga'] = DaftarHarga::all();

        return view("cms.jadwal.edit",$data);

    }

    public function update(JadwalRequest $request,$id){
        $jadwal = Jadwal::find($id);
        $jadwal->guru_id = $request->input("guru_id");
        $jadwal->asisten_id = $request->input("asisten_id");
        $jadwal->waktu = $request->input("waktu");
        $jadwal->tanggal = $request->input("tanggal");
        $jadwal->tempat = $request->input("tempat");
        $jadwal->daftar_harga_id = $request->input("daftar_harga_id");
        $jadwal->save();

        return redirect("cms/jadwal")->with("success","Jadwal berhasil diubah");
    }

    public function delete($id){
        $daftarharga = DaftarHarga::find($id)->delete();
        return redirect("cms/jadwal")->with("success","Jadwal berhasil dihapus");
    }
}
