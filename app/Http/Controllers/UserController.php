<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "User";
        $data['users'] = User::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $email = "";
            $name = "";
            $type = "";

            if (isset($_GET['email'])) {
                if (!empty($_GET['email'])) {
                    $email = trim($_GET['email']);
                }
            }

            if (isset($_GET['name'])) {
                if (!empty($_GET['name'])) {
                    $name = trim($_GET['name']);
                }
            }

            if (isset($_GET['type'])) {
                if (!empty($_GET['type']) && $_GET['type'] != "null") {
                    $type = trim($_GET['type']);
                }
            }

            $users = User::search($email,$name,$type)->paginate(10);
            $output['users'] = view("cms.user.user", ['users' => $users])->render();
            $output['ul_users'] = view("cms.user.ul", ['users' => $users])->render();

            return response()->json($output, 200);
        }

        return view("cms.user.index",$data);
    }

    public function show($id){
        $user = User::find($id);
        $output=[];
        $output['name'] = $user->name;
        $output['email'] = $user->email;
        $output['created_at'] = (string) $user->created_at;
        $output['type'] = $user->type;

        return response()->json($output,200);
    }

    public function create(){
        $data['title'] = "Create User";

        return view("cms.user.create",$data);
    }

    public function store(UserRequest $request){
        $user = new User();
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->type = "admin";
        $user->password = bcrypt($request->input("password"));
        $user->save();

        return redirect("cms/users")->with("success","User has been added.");
    }

    public function edit($id){
        $data['title'] = "Edit User";
        $data['user'] = User::find($id);

        return view("cms.user.edit",$data);

    }

    public function update(UserRequest $request,$id){
        $user = User::find($id);
        $user->name = $request->input("name");
        $user->save();

        return redirect("cms/users")->with("success","User has been changed.");
    }

    public function delete($id){
        $user = User::find($id)->delete();
        return redirect("cms/users")->with("success","User has been deleted.");
    }
}
