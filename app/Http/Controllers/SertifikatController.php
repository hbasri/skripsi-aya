<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sertifikat;
use App\Pemesanan;
use App\Http\Requests\SertifikatRequest;
use Image;
use DB;
use Illuminate\Support\Facades\Storage;

class SertifikatController extends Controller
{
    public function index(Request $request){
        $data['title'] = "Sertifikat";
        $data['sertifikat'] = Sertifikat::orderBy("id","desc")->paginate(10);
        $data['pemesanan'] = Pemesanan::all();

        if ($request->ajax()) {
            $order_id = "";

            if (isset($_GET['order_id'])) {
                if (!empty($_GET['order_id'])) {
                    $order_id = trim($_GET['order_id']);
                }
            }

            $sertifikat = Sertifikat::search($nama)->paginate(10);
            $output['sertifikat'] = view("cms.order_id.sertifikat", ['sertifikat' => $sertifikat])->render();
            $output['ul_sertifikat'] = view("cms.sertifikat.ul", ['sertifikat' => $sertifikat])->render();

            return response()->json($output, 200);
        }

        return view("cms.sertifikat.index",$data);
    }

    public function create(){
        $data['title'] = "Create Sertifikat";
        $data['pemesanan'] = Pemesanan::all();

        return view("cms.sertifikat.create",$data);
    }

    public function store(SertifikatRequest $request){
        try{
            DB::beginTransaction();
            $sertifikat = new Sertifikat();
            $sertifikat->order_id = $request->input("order_id");

            if($request->hasFile("files")){
                $img = $request->file("files");
                $image = Image::make($img);
//            $image->fit(250);
                $img_name = "picture".uniqid().'.jpg';
                $image->save(public_path('assets/sertifikat/'.$img_name));
                $sertifikat->files = $img_name;
            }

            $sertifikat->save();
            DB::commit();

            return redirect("cms/sertifikat")->with("success","Sertifikat Berhasil Ditambah.");
        }catch (\Exception $e){
            DB::rollback();
            dd($e);

        }

    }

    public function edit($id){
        $data['title'] = "Edit Sertifikat";
        $data['pemesanan'] = Pemesanan::all();
        $data['sertifikat'] = Sertifikat::find($id);

        return view("cms.sertifikat.edit",$data);

    }

    public function update(Sertifikat $request,$id){
        try{
            DB::beginTransaction();
            $sertifikat = Sertifikat::find($id);
            $sertifikat->order_id = $request->input("order_id");

            if($request->hasFile("files")){
                $img = $request->file("files");
                $image = Image::make($img);
//            $image->fit(250);
                $img_name = "picture".uniqid().'.jpg';
                $image->save(public_path('assets/sertifikat/'.$img_name));
                $sertifikat->files = $img_name;
                Storage::delete(public_path('assets/sertifikat/".$request->input("files_path")'));
            }

            $sertifikat->save();
            DB::commit();

            return redirect("cms/sertifikat")->with("success","Sertifikat berhasil diubah.");
        }catch (\Exception $e){
            DB::rollback();
            $errors = [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "code" => $e->getCode(),
                "file" => $e->getFile()
            ];

            DB::rollback();

            return response()->json($errors,400);

        }
    }

    public function delete($id){
        Sertifikat::find($id)->delete();
        return redirect("cms/sertifikat")->with("success","Sertifikat Berhasil Dihapus.");
    }
}
