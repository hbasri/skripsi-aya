<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Requests\UserRequest;

class AdminController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "Admin";
        $data['users'] = Admin::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $username = "";
            $name = "";

            if (isset($_GET['username'])) {
                if (!empty($_GET['username'])) {
                    $username = trim($_GET['username']);
                }
            }

            if (isset($_GET['name'])) {
                if (!empty($_GET['name'])) {
                    $name = trim($_GET['name']);
                }
            }

            $users = Admin::search($username,$name)->paginate(10);
            $output['users'] = view("cms.user.user", ['users' => $users])->render();
            $output['ul_users'] = view("cms.user.ul", ['users' => $users])->render();

            return response()->json($output, 200);
        }

        return view("cms.user.index",$data);
    }

    public function show($id){
        $user = Admin::find($id);
        $output=[];
        $output['name'] = $user->name;
        $output['username'] = $user->username;
        $output['created_at'] = (string) $user->created_at;

        return response()->json($output,200);
    }

    public function create(){
        $data['title'] = "Create Admin";

        return view("cms.user.create",$data);
    }

    public function store(UserRequest $request){
        $user = new Admin();
        $user->name = $request->input("name");
        $user->username = $request->input("username");
        $user->password = bcrypt($request->input("password"));
        $user->save();

        return redirect("cms/users")->with("success","Admin berhasil ditambah.");
    }

    public function edit($id){
        $data['title'] = "Edit Admin";
        $data['user'] = Admin::find($id);

        return view("cms.user.edit",$data);

    }

    public function update(UserRequest $request,$id){
        $user = Admin::find($id);
        $user->name = $request->input("name");
        $user->save();

        return redirect("cms/users")->with("success","Admin berhasil diubah.");
    }

    public function delete($id){
        $user = Admin::find($id)->delete();
        return redirect("cms/users")->with("success","Admin berhasil dihapus.");
    }
}
