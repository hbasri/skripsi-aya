<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use Auth;
use App\Admin;
use Illuminate\Support\Facades\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','logoutCms');
    }

    public function showLoginForm(){
        $data['title'] = "Login";
        return view("login",$data);
    }

    public function showLoginFormCms(){
        $data['title'] = "CMS";
        return view("cms.login",$data);
    }

    public function login(LoginRequest $request){
        try{
            $findByUsername = Admin::where("username",$request->input("username"))->first();

            if(count($findByUsername) > 0){
                if(Auth::attempt(["username"=>$request->input("username"),"password"=>$request->input("password")],($request->has("rememberme") ? true:false))){
                    Admin::where("username",$request->input("username"))->update(["last_login"=>date("Y-m-d H:i:s")]);

                    if ($findByEmail->type == "admin") {
                        return response()->json(['status' => false, "error_message" => "Something went wrong"],400);
                    }

                    return response()->json(['status' => true,'payload' => "/review/write"]);
                }

                return response()->json(['status' => false, "error_message" => "Email or password is wrong"],400);
            }

            return response()->json(['status' => false, "error_message" => "Email not found"],400);

        }catch (\Exception $e){
            $errors = [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "code" => $e->getCode(),
                "file" => $e->getFile()
            ];

            return response()->json($errors,400);
        }
    }

    public function loginCms(LoginRequest $request){
        try{
            $findByUsername = Admin::where("username",$request->input("username"))->first();

            if(count($findByUsername) > 0){
                if(Auth::attempt(["username"=>$request->input("username"),"password"=>$request->input("password")],($request->has("rememberme") ? true:false))){
                    Admin::where("username",$request->input("username"))->update(["last_login"=>date("Y-m-d H:i:s")]);

                    return redirect("cms/home");
                }

                return back()->withInput()->with(["error" => "Username or password is wrong"]);
            }

            return back()->withInput()->with(["error" => "Username not found"]);

        }catch (\Exception $e){
            $errors = [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "code" => $e->getCode(),
                "file" => $e->getFile()
            ];

            return response()->json($errors,400);
        }
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect("/")->with(['success'=>'Success Logout.']);
    }

    public function logoutCms(Request $request){
        Auth::logout();
        return redirect("/cms/login")->with(['success'=>'Success Logout.']);
    }
}
