<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $data['title'] = "Register";
        return view("register",$data);
    }

    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),
                [
                    "name" => "required|max:255",
                    "email" => "required|unique:users,email|email|max:255",
                    "password" => "required|max:255",
                    "publisher_name" => "required|max:255"
                ],
                [
                    "name.required" => "Name must be filled",
                    "name.max" => "Name max :max character",
                    "email.required" => "Email must be filled",
                    "email.unique" => "Email :unique already exist",
                    "email.email" => "Email :email format wrong",
                    "email.max" => "Email max :max character",
                    "publiser_name.required" => "Publisher must be filled",
                    "publisher_name.max" => "Publisher max :max character"
                ]
            );

            if($validator->fails()){
                return response()->json(["status" => false, "payload" => $validator->errors()], 400);
            }

            $user = new User();
            $user->name = $request->input("name");
            $user->email = $request->input("email");
            $user->publisher_name = $request->input("publisher_name");
            $user->password = bcrypt($request->input("password"));
            $user->type = "user";
            $user->save();

            return response()->json(['status' => true ,'payload' => '/register/thanks']);

        }catch (\Exception $e){
            $errors = [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "code" => $e->getCode(),
                "file" => $e->getFile()
            ];

            return response()->json($errors,400);
        }
    }

    public function thanks(){
        $data['title'] = "Thanks";
        return view("thanks",$data);
    }

}
