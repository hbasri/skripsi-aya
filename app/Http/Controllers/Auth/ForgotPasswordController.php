<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function testEmail(){
        try{
            $obj = new \stdClass();
            $obj->email = "hasanbasri2307@gmail.com";
            $obj->name = "Hasan Basri";

            Mail::to("hasanbasri2307@outlook.com")->send(new ForgotPassword());
            if( count(Mail::failures()) > 0 ) {

                echo "There was one or more failures. They were: <br />";



            } else {
                echo "No errors, all sent successfully!";
            }
//            dd('Mail Send Successfully');
        }catch (\Exception $e){
            dd($e);
        }



    }
}
