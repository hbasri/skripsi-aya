<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DaftarHarga;
use App\Review;
use App\Customer;
use App\Pemesanan;
use App\Jadwal;
use App\StatusPembayaran;
use App\GuruAsisten;
use App\Sertifikat;
use Auth;
use DB;
use PDF;

class FrontController extends Controller
{
    
    public function index(){
    	return view("front.index");
    }

    public function daftarHarga(){
    	$data['daftarHarga'] = DaftarHarga::all();
    	return view("front.daftar_harga",$data);
    }

    public function review(){
    	$data['reviews'] = Review::all();
    	return view("front.review",$data);
    }

    public function tambahReview(){
    	return view("front.tambah_review");
    }

    public function postTambahReview(Request $request){
    	$review = new Review();
    	$review->nama = $request->input("nama");
    	$review->email = $request->input("email");
    	$review->tanggal_private = $request->input("tanggal_private");
    	$review->isi = $request->input("isi");
    	$review->save();

    	return redirect('review');
    }

    public function daftar(){
    	return view("front.daftar");
    }

    public function postDaftar(Request $request){
    	$customer = new Customer();
    	$customer->nama = $request->input("nama");
    	$customer->alamat = $request->input("alamat");
    	$customer->telpon = $request->input("telepon");
    	$customer->jenis_kelamin = $request->input("jenis_kelamin");
    	$customer->email = $request->input("email");
    	$customer->username = $request->input("username");
    	$customer->password = bcrypt($request->input("password"));
    	$customer->save();

    	return redirect("masuk"); 
    }

    public function masuk(){
    	return view("front.login");
    }

    public function postMasuk(Request $request){
		if (Auth::guard('customer')->attempt(['username' => $request->input("username"), 'password' => $request->input("password")],true)) {
		        // if successful, then redirect to their intended location
		    return redirect('/');
		}
    }

    public function postKeluar(){
    	Auth::guard("customer")->logout();

    	return redirect("/");
    }

    public function pemesanan(){
    	$data['pemesanan'] = Pemesanan::all();
    	return view("front.pemesanan",$data);
    }

    public function tambahPemesanan(){
    	$data['daftarharga'] = Jadwal::all();
    	return view("front.tambah_pemesanan",$data);
    }

    public function postPemesanan(Request $request){
    	try {
    		DB::beginTransaction();

    		$pemesanan = new Pemesanan();
	    	$pemesanan->nama_pemesan = $request->input("nama_pemesan");
	    	$pemesanan->customer_id = Auth::guard("customer")->user()->id;
	    	$pemesanan->order_date = date("Y-m-d");
	    	$pemesanan->order_status = "pending";
	    	$pemesanan->price = $request->input("jumlah_pembayaran");
	    	$pemesanan->tempat = $request->input("tempat_private");
	    	$pemesanan->schedule_id = $request->input("daftar_harga_id");
	    	$pemesanan->total_peserta = $request->input("jumlah");
	    	$pemesanan->save();

            $pemesanan->order_number = "ORD".str_pad($pemesanan->id, 4, "0", STR_PAD_LEFT);
            $pemesanan->save();

	    	$pembayaran = new StatusPembayaran();
	    	$pembayaran->payment_total = intval($request->input("jumlah_pembayaran"));
	    	$pembayaran->order_id = $pemesanan->id;
	    	$pembayaran->user_id = Auth::guard("customer")->user()->id;
	    	$pembayaran->save();

	    	DB::commit();
	    	return redirect("pemesanan");
    	}catch(\Exception $e){
    		DB::rollback();
    		return response()->json(['error' => $e->getMessage()]);
    	}
    	
    }

    public function getDaftarHarga($id){
    	$daftarharga = Jadwal::with("daftarHarga")->find($id);
    	return response()->json(['status' => true,'paket' => $daftarharga]);
    }

    public function pembayaran(){
    	$data['pembayaran'] = StatusPembayaran::where("user_id",Auth::guard("customer")->user()->id)->get();
    	return view("front.pembayaran",$data);
    }

    public function daftarGuru(){
        $data['guruasisten'] = GuruAsisten::all();
        $data['jadwal'] = Jadwal::all();
        return view("front.guruasisten",$data);
    }

    public function sertifikat(){
        $data['sertifikat'] = DB::table("sertifikat")
                                ->join("pemesanan","pemesanan.id","=","sertifikat.order_id")
                                ->select("*")
                                ->where("pemesanan.customer_id",Auth::guard("customer")->user()->id)
                                ->whereNull("sertifikat.deleted_at")
                                ->get();

        return view("front.sertifikat",$data);
    }

    public function panduan(){
        return view("front.panduan");
    }

    public function qa(){
        return view("front.qa");
    }

    public function gallery(){
        return view("front.gallery");
    }

    public function contact(){
        return view("front.contact");
    }

    public function cetakStatusPembayaran($id){
        $data['pembayaran'] = StatusPembayaran::find($id);
        $pdf = PDF::loadView('cms.pembayaran.invoice', $data)->setPaper('a4','potrait');
        return $pdf->stream('result.pdf'); 
    }
}
