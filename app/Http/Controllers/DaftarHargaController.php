<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DaftarHarga;
use App\Http\Requests\DaftarHargaRequest; 

class DaftarHargaController extends Controller
{
    //
    //
    public function index(Request $request){
        $data['title'] = "Daftar Harga";
        $data['daftarharga'] = DaftarHarga::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $nama = "";
            $harga = "";
            $deskripsi = "";

            if (isset($_GET['nama'])) {
                if (!empty($_GET['nama'])) {
                    $nama = trim($_GET['nama']);
                }
            }

            if (isset($_GET['harga'])) {
                if (!empty($_GET['harga'])) {
                    $harga = trim($_GET['harga']);
                }
            }

            if (isset($_GET['deskripsi'])) {
                if (!empty($_GET['deskripsi'])) {
                    $deskripsi = trim($_GET['deskripsi']);
                }
            }

            $daftargarga = DaftarHarga::search($nama,$harga,$deskripsi)->paginate(10);
            $output['daftarharga'] = view("cms.daftarharga.daftarharga", ['daftarharga' => $daftarharga])->render();
            $output['ul_daftarharga'] = view("cms.daftarharga.ul", ['daftarharga' => $daftarharga])->render();

            return response()->json($output, 200);
        }

        return view("cms.daftarharga.index",$data);
    }

    public function show($id){
        $daftarharga = DaftarHarga::find($id);
        $output=[];
        $output['nama'] = $daftarharga->nama;
        $output['harga'] = $daftarharga->harga;
        $output['deskripsi'] = $daftarharga->deskripsi;
        $output['created_at'] = (string) $daftarharga->created_at;

        return response()->json($output,200);
    }

    public function create(){
        $data['title'] = "Create Daftar Harga";

        return view("cms.daftarharga.create",$data);
    }

    public function store(DaftarHargaRequest $request){
        $daftarharga = new DaftarHarga();
        $daftarharga->nama = $request->input("nama");
        $daftarharga->harga = $request->input("harga");
        $daftarharga->deskripsi = $request->input("deskripsi");
        $daftarharga->save();

        return redirect("cms/daftar-harga")->with("success","Daftar harga berhasil ditambah");
    }

    public function edit($id){
        $data['title'] = "Edit Daftar Harga";
        $data['daftarharga'] = DaftarHarga::find($id);

        return view("cms.daftarharga.edit",$data);

    }

    public function update(DaftarHargaRequest $request,$id){
        $daftarharga = DaftarHarga::find($id);
        $daftarharga->nama = $request->input("nama");
        $daftarharga->harga = $request->input("harga");
        $daftarharga->deskripsi = $request->input("deskripsi");
        $daftarharga->save();

        return redirect("cms/daftar-harga")->with("success","Daftar harga berhasil diubah");
    }

    public function delete($id){
        $daftarharga = DaftarHarga::find($id)->delete();
        return redirect("cms/daftar-harga")->with("success","Daftar Harga berhasil dihapus");
    }
}
