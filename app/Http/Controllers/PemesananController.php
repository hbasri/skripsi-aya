<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemesanan;
use PDF;

class PemesananController extends Controller
{
    //
    public function index(Request $request){
        $data['title'] = "Pemesanan";
        $data['pemesanan'] = Pemesanan::orderBy("id","desc")->paginate(10);

        if ($request->ajax()) {
            $nama = "";
            $tanggal = "";
            $waktu = "";
            $tempat="";
            $jumlah="";

            if (isset($_GET['nama'])) {
                if (!empty($_GET['nama'])) {
                    $nama = trim($_GET['nama']);
                }
            }

            if (isset($_GET['tanggal'])) {
                if (!empty($_GET['tanggal'])) {
                    $tanggal = trim($_GET['tanggal']);
                }
            }

            if (isset($_GET['waktu'])) {
                if (!empty($_GET['waktu'])) {
                    $waktu = trim($_GET['waktu']);
                }
            }

            if (isset($_GET['tempat'])) {
                if (!empty($_GET['tempat'])) {
                    $tempat = trim($_GET['tempat']);
                }
            }

            if (isset($_GET['jumlah'])) {
                if (!empty($_GET['jumlah'])) {
                    $jumlah = trim($_GET['jumlah']);
                }
            }

            $pemesanan = Pemesanan::search($nama,$tanggal,$waktu,$tempat,$jumlah)->paginate(10);
            $output['pemesanan'] = view("cms.pemesanan.pemesanan", ['pemesanan' => $pemesanan])->render();
            $output['ul_pemesanan'] = view("cms.pemesanan.ul", ['pemesanan' => $pemesanan])->render();

            return response()->json($output, 200);
        }

        return view("cms.pemesanan.index",$data);
    }

    public function show($id){
        $daftarharga = DaftarHarga::find($id);
        $output=[];
        $output['nama'] = $daftarharga->nama;
        $output['harga'] = $daftarharga->harga;
        $output['deskripsi'] = $daftarharga->deskripsi;
        $output['created_at'] = (string) $daftarharga->created_at;

        return response()->json($output,200);
    }

    public function report(){
        $data['title'] = "Laporan Pemesanan";
        return view("cms.pemesanan.report",$data);
    }

    public function postReport(Request $request){
        $dari = $request->input("dari");
        $sampai = $request->input("sampai");

        $data['dari'] = $dari;
        $data['sampai'] = $sampai;
        $data['pemesanan'] = Pemesanan::report($dari,$sampai)->get();

        $pdf = PDF::loadView('cms.pemesanan.print_report', $data)->setPaper('a4','potrait');
        return $pdf->stream('result.pdf'); 
    }
}
