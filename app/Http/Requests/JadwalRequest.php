<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JadwalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "guru_id" => "required",
            "asisten_id" => "required",
            "waktu" => "required",
            "tanggal" => "required",
            "tempat" => "required"
        ];
    }

    public function messages(){
        return [
            "guru_id.required" => "Guru harus diisi",
            "asisten_id.required" => "Asisten harus diisi",
            "waktu.required" => "Waktu harus diisi",
            "tanggal.required" => "Tanggal harus diisi",
            "tempat.required" => "Tempat harus diisi"
        ];
    }
}
