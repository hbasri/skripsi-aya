<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "name" => "required|max:255",
            "email" => "required|unique|email|max:255",
            "password" => "required|max:255",
            "publisher_name" => "required:max|255"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => "Name must be filled",
            "name.max" => "Name max :max character",
            "email.required" => "Email must be filled",
            "email.unique" => "Email :unique already exist",
            "email.email" => "Email :email format wrong",
            "email.max" => "Email max :max character",
            "publiser_name.required" => "Publisher must be filled",
            "publisher_name.max" => "Publisher max :max character"
        ];
    }
}
