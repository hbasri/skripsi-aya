<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DaftarHargaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nama' => "required",
            "harga" => "required",
            "deskripsi" => "required"
        ];
    }

    public function messages(){
        return [
            "nama.required" => "Nama harus diisi",
            "harga.required" => "Harga harus diisi",
            "deskripsi.required" => "Deskripsi harus diisi"
        ];
    }
}
