<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuruAsistenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "nama" => "required",
            "jenis_kelamin" => "required",
            "kode_staff" => "required",
            "email" => "required|email",
            "tipe_staff" => "required",
            "pendidikan_terakhir" => "required",
            "instansi_pendidikan_terakhir" => "required"
        ];
    }

    public function messages(){
        return [
            "nama.required" => "Nama harus diisi",
            "jenis_kelamin.required" => "Jenis Kelamin harus diisi",
            "kode_staff.required" => "Kode Staff harus diisi",
            "email.required" => "Email harus diisi",
            "email.email" => "Format email salah",
            "tipe_staff.required" => "Tipe Staff harus diisi",
            "pendidikan_terakhir.required" => "Pendidikan Terakhir harus diisi",
            "instansi_pendidikan_terakhir.required" => "Instansi Pendidikan Terakhir harus diisi"
        ];
    }
}
