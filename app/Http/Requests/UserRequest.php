<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()){
            case 'POST' :
                return [
                    "username" => 'required|unique:admin,username',
                    "password" => 'required|confirmed',
                    "password_confirmation" => "required",
                    "name" => "required"
                ];
                break;

            case 'PATCH' :
                return [
                    "name" => "required"
                ];
        }
    }

    public function messages(){
        return [
            "username.requred" => "Username harus diisi",
            "username.unique" => "Username sudah terdaftar",
            "password.required" => "Password harus diisi",
            "password_confirmation.required" => "Password Confirmation harus diisi",
            "name.required" => "Nama harus diisi"
        ];
    }
}
