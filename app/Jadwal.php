<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Jadwal extends Model
{
    //
    use SoftDeletes;
    
    protected $table = "jadwal";
    protected $dates = ['deleted_at'];

    public function guru(){
    	return $this->belongsTo(GuruAsisten::class,"guru_id");
    }

    public function asisten(){
    	return $this->belongsTo(GuruAsisten::class,"asisten_id");
    }

    public function daftarHarga(){
        return $this->belongsTo(DaftarHarga::class,"daftar_harga_id");
    }

    public function scopeSearch($q,$guru,$asisten,$waktu,$tanggal,$tempat,$daftarharga){
        if(!empty($guru))
            if($guru != "all")
                $q->where("guru_id",$guru);

        if(!empty($asisten))
            if($asisten != "all")
                $q->where("asisten_id",$guru);


        if(!empty($daftarharga))
            if($daftarharga != "all")
                $q->where("daftar_harga_id",$guru);

        if(!empty($waktu))
            $q->where("waktu",$waktu);

        if(!empty($tanggal))
            $q->where("tanggal",$tanggal);

        if(!empty($tempat))
            $q->where("tempat",$tempat);

        $q->orderBy("id","desc");

        return $q;
    }
}
