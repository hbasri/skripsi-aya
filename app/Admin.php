<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Admin extends Authenticatable
{
    //
    use Notifiable,SoftDeletes;

    protected $fillable = [
        'name', 'username', 'password',
    ];

    protected $table = "admin";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearch($q,$username,$nama){
        if(!empty($username)){
            $q->where("username",$username);
        }

        if(!empty($nama)){
            $q->where("name",$nama);
        }


        return $q;
    }
}
