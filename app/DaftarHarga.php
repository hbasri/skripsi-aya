<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DaftarHarga extends Model
{
    //

    use SoftDeletes;
    
    protected $table = "daftar_harga";
    protected $dates = ['deleted_at'];

    public function scopeSearch($q,$nama,$harga,$deskripsi){
    	if(!empty($nama)){
    		$q->where("nama",$nama);
    	}

    	if(!empty($harga)){
    		$q->where("harga",$harga);
    	}

    	if(!empty($deskripsi)){
    		$q->where("deskripsi",$deskripsi);
    	}

    	return $q;
    }
}
