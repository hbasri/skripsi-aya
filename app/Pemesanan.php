<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemesanan extends Model
{
    //
    use SoftDeletes;
    
    protected $table = "pemesanan";
    protected $dates = ['deleted_at'];

    public function customer(){
    	return $this->belongsTo(Customer::class,"customer_id");
    }

    public function jadwal(){
    	return $this->belongsTo(Jadwal::class,"schedule_id");
    }

    public function pembayaran(){
        return $this->hasOne(StatusPembayaran::class,"order_id");
    }

    public function scopeSearch($q,$nama,$tanggal,$waktu,$tempat,$jumlah){

        if(!empty($nama)){
            $q->where("nama_pemesan",$nama);
        }

        if(!empty($tanggal) || !empty($waktu) || !empty($tempat)){
            $q->join("jadwal",function($join) use($tanggal,$waktu,$tempat){
                $join->on("pemesanan.schedule_id","=","jadwal.id");

                if(!empty($tanggal))
                    $join->where("tanggal",$tanggal);
                
                if(!empty($waktu))
                    $join->where("waktu",$waktu);

                if(!empty($tempat))
                    $join->where("tempat",$tempat);
            });
        }

        if(!empty($jumlah))
            $q->where("total_peserta",$jumlah);

        $q->orderBy("pemesanan.id","desc");

        return $q;
    }

    public function scopeReport($q,$dari,$sampai){
        $q->whereBetween("order_date",[$dari,$sampai]);

        $q->orderBy("id","desc");
        return $q;
    }

    public function sertifikat(){
        return $this->hasMany(Sertifikat::class,"order_id");
    }
}