<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusPembayaran extends Model
{
    //
    use SoftDeletes;
    
    protected $table = "status_pembayaran";
    protected $dates = ['deleted_at'];

    public function pemesanan(){
    	return $this->belongsTo(Pemesanan::class,"order_id");
    }

    public function customer(){
    	return $this->belongsTo(Customer::class,"user_id");
    }

    public function scopeSearch($q,$nama,$tanggal,$jumlah,$status){
        $q->join("pemesanan",function($join) use($nama){
            $join->on("pemesanan.id","=","status_pembayaran.order_id");

            if(!empty($nama)){
                $join->where("pemesanan.nama_pemesan","=",$nama);
            }
        });

        if(!empty($tanggal)) $q->where("payment_date",$tanggal);
        if(!empty($jumlah)) $q->where("payment_total",$jumlah);

        if(!empty($status)){
            if($status != "all"){
                $q->where("payment_status",$status);
            }
        }

        $q->orderBy("status_pembayaran.id","desc");

        return $q;
    }

    public function scopeReport($q,$dari,$sampai){
        $q->whereBetween("payment_date",[$dari,$sampai]);

        $q->orderBy("id","desc");
        return $q;
    }
}
