<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sertifikat extends Model
{
    //
    use SoftDeletes;

    protected $table = "sertifikat";
    protected $dates = ['deleted_at'];

    public function pemesanan(){
    	return $this->belongsTo(Pemesanan::class,"order_id");
    }

    public function scopeSearch($q,$order_id){

    	$q->join("pemesanan",function($join) use($order_id){
    		$join->on("pemesanan.id","=","sertifikat.order_id");

    		if(!empty($order_id)){
    			if($order_id != "all") $join->where("order_id",$order_id);	
	    	}
    	});

    	$q->orderBy("sertifikat.id","desc");

    	return $q;
    }

}
