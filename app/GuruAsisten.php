<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class GuruAsisten extends Model
{
    //
    use SoftDeletes;
    
    protected $table = "guruasisten";
    protected $dates = ['deleted_at'];

    public function scopeSearch($q,$nama,$jenis_kelamin,$kode_staff,$email,$tipe_staff,$pendidikan_terakhir,$instansi_pendidikan_terakhir){
    	if(!empty($nama)){
    		$q->where("nama",$nama);
    	}

    	if(!empty($jenis_kelamin)){
    		if($jenis_kelamin != "all"){
    			$q->where("jenis_kelamin",$jenis_kelamin);
    		}
    	}

    	if(!empty($kode_staff)){
    		$q->where("kode_staff",$kode_staff);
    	}

    	if(!empty($email)){
    		$q->where("email",$email);
    	}

    	if(!empty($tipe_staff)){
    		if($tipe_staff != "all"){
    			$q->where("tipe_staff",$tipe_staff);
    		}
    	}

    	if(!empty($pendidikan_terakhir)){
    		$q->where("pendidikan_terakhir",$pendidikan_terakhir);
    	}

    	if(!empty($instansi_pendidikan_terakhir)){
    		$q->where("instansi_pendidikan_terakhir",$instansi_pendidikan_terakhir);
    	}

    	return $q;
    }
}
