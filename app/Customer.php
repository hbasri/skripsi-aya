<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Authenticatable
{
    //
    use SoftDeletes;
    
     protected $table = "customer";
}

