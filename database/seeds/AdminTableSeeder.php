<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = new Admin();
        $admin->username = "demo";
        $admin->password = bcrypt("demo123");
        $admin->name = "Demo Account";
        $admin->save();
    }
}
