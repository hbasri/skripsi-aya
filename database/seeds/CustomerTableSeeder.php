<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customer = new Customer();
        $customer->nama = "Demo Customer";
        $customer->alamat = "Jakarta";
        $customer->telpon = "21212121";
        $customer->username = "demo";
        $customer->password = bcrypt("demo123");
        $customer->save();
    }
}
