<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get("/","FrontController@index");
Route::get("daftar-harga","FrontController@daftarHarga");
Route::get("review","FrontController@review");
Route::get("review/tambah","FrontController@tambahReview");
Route::post("review/tambah","FrontController@postTambahReview");
Route::get("daftar","FrontController@daftar");
Route::post("daftar","FrontController@postDaftar");
Route::get("masuk","FrontController@masuk");
Route::post("masuk","FrontController@postMasuk");
Route::get("keluar","FrontController@postKeluar");
Route::get("pemesanan","FrontController@pemesanan");
Route::get("pemesanan/tambah","FrontController@tambahPemesanan");
Route::post("pemesanan","FrontController@postPemesanan");
Route::get("daftarharga/{id}","FrontController@getDaftarHarga");
Route::get("pembayaran","FrontController@pembayaran");
Route::get("guru-asisten","FrontController@daftarGuru");
Route::get("sertifikat","FrontController@sertifikat");
Route::get("panduan","FrontController@panduan");
Route::get("qa","FrontController@qa");
Route::get("kontak","FrontController@contact");
Route::get("gallery","FrontController@gallery");
Route::get("invoice/{id}","FrontController@cetakStatusPembayaran");

//CMS
Route::get("/cms/login","Auth\LoginController@showLoginFormCms");
Route::post("/cms/login","Auth\LoginController@loginCms")->name("login");
Route::post("/cms/logout","Auth\LoginController@logoutCms");

Route::group(['middleware'=>'auth'],function(){
    Route::prefix('cms')->group(function () {

        Route::get("home","HomeController@index");

        //user
        Route::get("users","AdminController@index");
        Route::get("user/show/{id}","AdminController@show");
        Route::get("user/create","AdminController@create");
        Route::post("user","AdminController@store");
        Route::get("user/edit/{id}","AdminController@edit");
        Route::patch("user/{id}","AdminController@update");
        Route::delete("user/{id}","AdminController@delete");

        //daftar harga
        Route::get("daftar-harga","DaftarHargaController@index");
        Route::get("daftar-harga/show/{id}","DaftarHargaController@show");
        Route::get("daftar-harga/create","DaftarHargaController@create");
        Route::post("daftar-harga","DaftarHargaController@store");
        Route::get("daftar-harga/edit/{id}","DaftarHargaController@edit");
        Route::post("daftar-harga/{id}","DaftarHargaController@update");
        Route::delete("daftar-harga/{id}","DaftarHargaController@delete");

        //jadwal
        Route::get("jadwal","JadwalController@index");
        Route::get("jadwal/show/{id}","JadwalController@show");
        Route::get("jadwal/create","JadwalController@create");
        Route::post("jadwal","JadwalController@store");
        Route::get("jadwal/edit/{id}","JadwalController@edit");
        Route::post("jadwal/{id}","JadwalController@update");
        Route::delete("jadwal/{id}","JadwalController@delete");

        //guru asisten harga
        Route::get("guru-asisten","GuruAsistenController@index");
        Route::get("guru-asisten/show/{id}","GuruAsistenController@show");
        Route::get("guru-asisten/create","GuruAsistenController@create");
        Route::post("guru-asisten","GuruAsistenController@store");
        Route::get("guru-asisten/edit/{id}","GuruAsistenController@edit");
        Route::post("guru-asisten/{id}","GuruAsistenController@update");
        Route::delete("guru-asisten/{id}","GuruAsistenController@delete");

        //pemesanan
        Route::get("pemesanan","PemesananController@index");
        Route::get("pemesanan/laporan","PemesananController@report");
        Route::post("pemesanan/laporan","PemesananController@postReport");

        //pembayaran
        Route::post("pembayaran/create","StatusPembayaranController@store");
        Route::get("pembayaran","StatusPembayaranController@index");
        Route::get("pembayaran/laporan","StatusPembayaranController@report");
        Route::post("pembayaran/laporan","StatusPembayaranController@postReport");

        //sertifikat
        Route::get("sertifikat","SertifikatController@index");
        Route::get("sertifikat/create","SertifikatController@create");
        Route::post("sertifikat","SertifikatController@store");
        Route::get("sertifikat/edit/{id}","SertifikatController@edit");
        Route::post("sertifikat/{id}","SertifikatController@update");
        Route::delete("sertifikat/{id}","SertifikatController@delete");

    });
});