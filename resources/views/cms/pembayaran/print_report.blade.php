<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Aloha!</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }

    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%"  >
    <tr>
        <td align="right">
            <h3>Laporan Pembayaran Table Manner</h3>
            <pre>
                Jalan Pedongkelan Belakang Rt.05/16 No.37B Kel. Kapuk Kec.Cengkareng, Jakarta Barat
                Telepon & Whatsapp : 089921342135
            </pre>
        </td>
    </tr>

  </table>

  <table width="100%">
    <tr>
        <td><strong>Dari :</strong> {{ $dari }}</td>
        <td><strong>Sampai :</strong> {{ $sampai }}</td>
    </tr>

  </table>

  <br/>

  <table width="100%" cellpadding="10">
    <thead style="background-color: lightgray;">
      <tr>
          <th>Kode Pemesanan</th>
          <th>Nama Pemesanan</th>
          <th>Tanggal Bayar</th>
          <th>Jumlah Bayar</th>
          <th>Status Bayar</th>
      </tr>
    </thead>
    <tbody>
        @if(count($pembayaran) > 0)
        <?php $total=0;?>
    @foreach($pembayaran as $data)
        <?php $total+= $data->payment_total;?>
        <tr>
            <td>{{ $data->pemesanan->order_number }}</td>
            <td>{{ (!empty($data->pemesanan->nama_pemesan) ? $data->pemesanan->nama_pemesan : "-") }}</td>
            <td>{{ !empty($data->payment_date) ? $data->payment_date : "-" }}</td>
            <td>{{ number_format($data->payment_total) }}</td>
            <td>{!! $data->payment_status == "belum lunas" ? '<span class="label label-danger">Belum Lunas</span>
  ' : '<span class="label label-success">Lunas</span>
  ' !!}</td>
        </tr>
    @endforeach
  @else
    <tr>
        <td colspan="5">Data Not Found</td>
    </tr>
  @endif
      
    </tbody>

    <tfoot>
        <tr>
          <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">Total Bayar : Rp {{ number_format($total) }}</td>
          </tr>
    </tfoot>
  </table>

</body>
</html>