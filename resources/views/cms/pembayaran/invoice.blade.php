<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Aloha!</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td align="right">
            <h3>Table Manner Report</h3>
            <pre>
                Jalan Pedongkelan Belakang Rt.05/16 No.37B Kel. Kapuk Kec.Cengkareng, Jakarta Barat
                Telepon & Whatsapp : 089921342135
            </pre>
        </td>
    </tr>

  </table>

  <br/>

  <table width="100%" cellpadding="10">
    <tbody>
      <tr>
        <td style="background-color: gray;" width="20%">Kode Pemesanan</td>
        <td align="left" width="5%" style="background-color: gray;">:</td>
        <td align="left" style="background-color: lightgray;">{{ $pembayaran->pemesanan->order_number }}</td>
      </tr>
      <tr>
        <td style="background-color: gray;" width="20%">Nama Pemesanan</td>
        <td align="left" width="5%" style="background-color: gray;">:</td>
        <td align="left" style="background-color: lightgray;">{{ $pembayaran->pemesanan->nama_pemesan }}</td>
      </tr>
      <tr>
        <td style="background-color: gray;" width="20%">Jumlah Bayar</td>
        <td align="left" width="5%" style="background-color: gray;">:</td>
        <td align="left" style="background-color: lightgray;">Rp {{ number_format($pembayaran->payment_total) }}</td>
      </tr>
      <tr>
        <td style="background-color: gray;" width="20%">Status Bayar</td>
        <td align="left" width="5%" style="background-color: gray;">:</td>
        <td align="left" style="background-color: lightgray;">{{ $pembayaran->payment_status }}</td>
      </tr>
    </tbody>

  </table>

</body>
</html>