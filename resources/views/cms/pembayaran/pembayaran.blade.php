@if(count($pembayaran) > 0)
    @foreach($pembayaran as $data)
        <tr>
            <td>{{ (!empty($data->pemesanan->nama_pemesan) ? $data->pemesanan->nama_pemesan : "-") }}</td>
            <td>{{ !empty($data->payment_date) ? $data->payment_date : "-" }}</td>
            <td>{{ number_format($data->payment_total) }}</td>
            <td>{!! $data->payment_status == "belum lunas" ? '<span class="label label-danger">Belum Lunas</span>
' : '<span class="label label-success">Lunas</span>
' !!}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="4">Data Not Found</td>
    </tr>
@endif