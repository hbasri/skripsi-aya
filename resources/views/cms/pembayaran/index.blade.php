@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Pembayaran
            <small>Pembayaran Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Pembayaran</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pembayaran</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20%">Nama Pemesanan</th>
                                <th width="20%">Tanggal Bayar</th>
                                <th width="20%">Jumlah Bayar</th>
                                <th width="20%">Status Bayar</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchNama" class="form-control" placeholder="Nama Pemesan"></td>
                                <td><input type="text" name="searchTanggal" class="form-control datepicker" placeholder="Tanggal Bayar"> </td>
                                <td><input type="text" name="searchJumlah" class="form-control" placeholder="Jumlah Bayar"> </td>
                                <td>
                                    <select name="searchStatus" class="form-control">
                                        <option value="all">Semua</option>
                                        <option value="belum lunas">Belum Lunas</option>
                                        <option value="lunas">Lunas</option>
                                    </select>
                                </td>
                            </tr>
                            </thead>
                            <tbody id="pembayaran-body">
                                @include("cms.pembayaran.pembayaran")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="pembayaran-paginate">
                        @include("cms.pembayaran.ul")
                    </div>
                </div>
                <!-- /.box -->
                
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPembayaran(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getPembayaran($(this).attr('href').split('page=')[1]);
            });
        });

        function getPembayaran(page) {
            $("#pembayaran-body").html("<tr align='center'><td colspan='4'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&nama=" + $("input[name='searchNama']").val() + "&tanggal=" + $("input[name='searchTanggal']").val() +"&jumlah=" + $("input[name='searchJumlah']").val() + "&status=" + $("select[name='searchStatus']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#pembayaran-body').html(data.pembayaran);
                $("#pembayaran-paginate").html(data.ul_pembayaran);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[name='searchNama']").on("blur",function(){
                getPembayaran(1);
            });


            $("input[name='searchTanggal']").datepicker().on('changeDate', function (ev) {
                getPembayaran(1);
            });

            $("input[name='searchJumlah']").on("blur",function(){
                getPembayaran(1);
            });

            $("select[name='searchStatus']").on("change",function(){
                getPembayaran(1);
            });
        });

    </script>
@endsection