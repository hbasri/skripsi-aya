@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/iCheck/all.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Admin
            <small>Admin Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Admin</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Tambah Admin</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/user') }}" method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" name="name" value="{{ old("name") }}" class="form-control" placeholder="Nama">
                                @if($errors->has("name"))
                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username">
                                @if($errors->has("username"))
                                    <span class="help-block">{{ $errors->first("username") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                @if($errors->has("password"))
                                    <span class="help-block">{{ $errors->first("password") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Password Confirmation</label>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation">
                                @if($errors->has("password_confirmation"))
                                    <span class="help-block">{{ $errors->first("password_confirmation") }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });

    </script>
@endsection