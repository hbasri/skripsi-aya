@if(count($users) > 0)
    @foreach($users as $user)
        <tr>
            <td>{{ (!empty($user->name) ? $user->name : "-") }}</td>
            <td>{{ !empty($user->username) ? $user->username : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("cms/user/edit/".$user->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("cms/user/".$user->id) }}"
                               data-method="delete" data-confirm="Hapus data ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="3">Data Not Found</td>
    </tr>
@endif