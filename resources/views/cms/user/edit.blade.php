@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/iCheck/all.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Admin
            <small>Data Admin</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Admin</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Ubah Admin</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/user/'.$user->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Name">
                                @if($errors->has("name"))
                                    <span class="help-block">{{ $errors->first("name") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" name="text" value="{{ $user->username }}" class="form-control" placeholder="Username" disabled>

                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });

    </script>
@endsection