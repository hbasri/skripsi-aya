@if(count($daftarharga) > 0)
    @foreach($daftarharga as $data)
        <tr>
            <td>{{ (!empty($data->nama) ? $data->nama : "-") }}</td>
            <td>{{ !empty($data->harga) ? 'Rp ' .number_format($data->harga) : "-" }}</td>
            <td>{{ !empty($data->deskripsi) ? $data->deskripsi : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("cms/daftar-harga/edit/".$data->id) }}">Ubah</a>
                        {{--</li>--}}
                        <li><a href="{{ url("cms/daftar-harga/".$data->id) }}"
                               data-method="delete" data-confirm="Hapus data ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="4">Data Not Found</td>
    </tr>
@endif