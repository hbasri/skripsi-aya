@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Daftar Harga
            <small>Daftar Harga Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Daftar Harga</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Ubah Daftar Harga</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/daftar-harga/'.$daftarharga->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('nama') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" name="nama" value="{{ $daftarharga->nama }}" class="form-control" placeholder="Nama">
                                @if($errors->has("nama"))
                                    <span class="help-block">{{ $errors->first("nama") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('harga') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Harga</label>
                                <input type="number" name="harga" value="{{ $daftarharga->harga }}" class="form-control" placeholder="Harga">
                                @if($errors->has("harga"))
                                    <span class="help-block">{{ $errors->first("harga") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('deskripsi') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" placeholder="Deskripsi">{{ $daftarharga->deskripsi }}</textarea>
                                
                                @if($errors->has("deskripsi"))
                                    <span class="help-block">{{ $errors->first("deskripsi") }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

        });

    </script>
@endsection