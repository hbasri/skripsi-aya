@if(count($jadwal) > 0)
    @foreach($jadwal as $data)
        <tr>
            <td>{{ $data->daftarHarga->nama }}</td>
            <td>{{ (!empty($data->guru_id) ? $data->guru->nama : "-") }}</td>
            <td>{{ !empty($data->asisten_id) ? $data->asisten->nama : "-" }}</td>
            <td>{{ !empty($data->waktu) ? $data->waktu : "-" }}</td>
            <td>{{ !empty($data->tanggal) ? $data->tanggal : "-" }}</td>
            <td>{{ !empty($data->tempat) ? $data->tempat : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("cms/jadwal/edit/".$data->id) }}">Ubah</a>
                        {{--</li>--}}
                        <li><a href="{{ url("cms/jadwal/".$data->id) }}"
                               data-method="delete" data-confirm="Hapus data ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="7">Data Not Found</td>
    </tr>
@endif