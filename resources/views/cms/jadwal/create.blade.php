@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
     <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Jadwal
            <small>Jadwal Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Jadwal</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Tambah Jadwal</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/jadwal') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('guru_id') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Guru</label>
                                <select class="form-control" name="guru_id">
                                    @foreach($guru as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                    @endforeach
                                 </select>
                                
                                @if($errors->has("guru_id"))
                                    <span class="help-block">{{ $errors->first("nama") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('asisten_id') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Asisten</label>
                                
                                <select class="form-control" name="asisten_id">
                                    @foreach($asisten as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                    @endforeach
                                 </select>

                                @if($errors->has("asisten_id"))
                                    <span class="help-block">{{ $errors->first("asisten_id") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('daftar_harga_id') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Daftar Harga</label>
                                
                                <select class="form-control" name="daftar_harga_id">
                                    @foreach($daftarharga as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                    @endforeach
                                 </select>

                                @if($errors->has("daftar_harga_id"))
                                    <span class="help-block">{{ $errors->first("daftar_harga_id") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('tanggal') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tanggal</label>
                                <input type="text" name="tanggal" value="{{ old("tanggal") }}" class="form-control datepicker" placeholder="Tanggal">
                                @if($errors->has("tanggal"))
                                    <span class="help-block">{{ $errors->first("tanggal") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('waktu') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Waktu</label>
                                <input type="text" name="waktu" value="{{ old("waktu") }}" class="form-control" placeholder="Waktu">
                                @if($errors->has("waktu"))
                                    <span class="help-block">{{ $errors->first("waktu") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('tempat') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tempat</label>
                                <input type="text" name="tempat" value="{{ old("tempat") }}" class="form-control" placeholder="Tempat">
                                @if($errors->has("tempat"))
                                    <span class="help-block">{{ $errors->first("tempat") }}</span>
                                @endif
                            </div>
                            
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });

        });

    </script>
@endsection