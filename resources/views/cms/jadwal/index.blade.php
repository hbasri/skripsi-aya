@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Jadwal
            <small>Jadwal Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Jadwal</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Jadwal</span>

                            <div class="margin">
                                <a href="{{ url("cms/jadwal/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-users"></i>
                                        Jadwal
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Jadwal</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20%">Daftar Harga</th>
                                <th width="20%">Guru</th>
                                <th width="20%">Asisten</th>
                                <th width="10%">Waktu</th>
                                <th width="10%">Tanggal</th>
                                <th width="10%">Tempat</th>
                                <th width="10%">Action</th>
                            </tr>
                            <tr>
                                <td>
                                    <select name="searchDaftarHarga" class="form-control">
                                        <option value="all">Semua</option>
                                        @foreach($daftarharga as $data)
                                            <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                </td>
                                <td>
                                    <select name="searchGuruId" class="form-control">
                                        <option value="all">Semua</option>
                                        @foreach($guru as $data)
                                            <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                </td>
                                <td>
                                    <select name="searchAsistenId" class="form-control">
                                        <option value="all">Semua</option>
                                        @foreach($asisten as $data)
                                            <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                        @endforeach
                                </td>
                                <td><input type="text" name="searchWaktu" class="form-control" placeholder="Waktu"> </td>
                                <td><input type="text" name="searchTanggal" class="form-control datepicker" placeholder="Tanggal"> </td>
                                <td><input type="text" name="searchTempat" class="form-control datepicker" placeholder="Tempat"></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="jadwal-body">
                                @include("cms.jadwal.jadwal")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="jadwal-paginate">
                        @include("cms.jadwal.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getJadwal(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getJadwal($(this).attr('href').split('page=')[1]);
            });
        });

        function getJadwal(page) {
            $("#jadwal-body").html("<tr align='center'><td colspan='7'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&guruid=" + $("select[name='searchGuruId']").val() + "&asistenid=" + $("select[name='searchAsistenId']").val() +"&waktu=" + $("input[name='searchWaktu']").val() + "&tanggal=" +$("input[name='searchTanggal']").val() + "&tempat=" +$("input[name='searchTempat']").val() + "&daftarharga=" + $("select[name='searchDaftarHarga']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#jadwal-body').html(data.jadwal);
                $("#jadwal-paginate").html(data.ul_jadwal);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[getJadwal='searchWaktu']").on("blur",function(){
                getJadwal(1);
            });

            $("input[name='searchTanggal']").on("blur",function(){
                getJadwal(1);
            });

            $("input[name='searchTempat']").on("blur",function(){
                getJadwal(1);
            });

            $("select[name='searchGuruId']").on("change",function(){
                getJadwal(1);
            });

            $("select[name='searchAsistenId']").on("change",function(){
                getJadwal(1);
            });

            $("select[name='searchDaftarHarga']").on("change",function(){
                getJadwal(1);
            });
        });

    </script>
@endsection