@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Reviews
            <small>Reviews Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Reviews</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        {{--<div class="button-action">--}}
                            {{--<span class="title-action">Add User</span>--}}

                            {{--<div class="margin">--}}
                                {{--<a href="{{ url("cms/user/create") }}">--}}
                                    {{--<button type="button" class="btn btn-primary"><i class="fa fa-fw fa-users"></i>--}}
                                        {{--User--}}
                                    {{--</button>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Reviews</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10%">Publisher Name</th>
                                <th width="10%">Review Date</th>
                                <th width="5%">Rate</th>
                                <th width="10%">Movie</th>
                                <th width="20%">Content</th>
                                <th width="5%">Action</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchPublisherName" class="form-control" placeholder="Publisher Name"></td>
                                <td><input type="text" name="searchReviewDate" class="form-control datepicker" placeholder="Date"> </td>
                                <td><input type="text" name="searchRate" class="form-control" placeholder="Rate"> </td>
                                <td>
                                    <select name="searchMovie" class="form-control select2">
                                        <option disabled selected>Choose</option>
                                        <option value="all">All</option>
                                        @foreach($movies as $movie)
                                            <option value="{{ $movie->id }}">{{ $movie->title }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input type="text" name="searchContent" class="form-control" placeholder="content"> </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="reviews-body">
                                @include("cms.review.review")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="reviews-paginate">
                        @include("cms.review.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getReviews(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getReviews($(this).attr('href').split('page=')[1]);
            });
        });

        function getReviews(page) {
            $("#reviews-body").html("<tr align='center'><td colspan='6'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&publisher_name=" + $("input[name='searchPublisherName']").val() + "&content=" + $("input[name='searchContent']").val() + "&movie_id=" + $("select[name='searchMovie']").val() +"&rate=" + $("input[name='searchRate']").val() + "&review_date=" + $("input[name='searchReviewDate']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#reviews-body').html(data.reviews);
                $("#reviews-paginate").html(data.ul_reviews);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[name='searchPublisherName']").on("blur",function(){
                getReviews(1);
            });

            $("input[name='searchContent']").on("blur",function(){
                getReviews(1);
            });

            $("input[name='searchRate']").on("blur",function(){
                getReviews(1);
            });

            $("input[name='searchReviewDate']").on("blur",function(){
                getReviews(1);
            });

            $("select[name='searchMovie']").on("change",function(){
                getReviews(1);
            });

        });

    </script>
@endsection