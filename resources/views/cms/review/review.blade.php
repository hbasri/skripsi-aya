@if(count($reviews) > 0)
    @foreach($reviews as $review)
        <tr>
            <td>{{ (!empty($review->user->publisher_name) ? $review->user->publisher_name : "-") }}</td>
            <td>{{ !empty($review->review_date) ? $review->review_date : "-" }}</td>
            <td>{{ !empty($review->rate) ? $review->rate : "-" }}</td>
            <td>{{ !empty($review->movie->title) ? $review->movie->title : "-" }}</td>
            <td>{{ !empty($review->content) ? $review->content : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        {{--<li><a href="{{ url("cms/user/edit/".$user->id) }}">Change</a>--}}
                        {{--</li>--}}
                        <li><a href="{{ url("cms/review/".$review->id) }}"
                               data-method="delete" data-confirm="Delete this data?"
                               data-token="{{ csrf_token() }}">Delete</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">Data Not Found</td>
    </tr>
@endif