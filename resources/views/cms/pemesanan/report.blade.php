@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
     <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Laporan Pemesanan
            <small>Laporan Pemesanan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Laporan Pemesanan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Laporan Pemesanan</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/pemesanan/laporan') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('dari') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Dari Tanggal</label>
                                <input type="text" name="dari" value="{{ old("dari") }}" class="form-control datepicker" placeholder="Dari Tanggal" required>
                                @if($errors->has("dari"))
                                    <span class="help-block">{{ $errors->first("dari") }}</span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('sampai') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Sampai Tanggal</label>
                                <input type="text" name="sampai" value="{{ old("sampai") }}" class="form-control datepicker" placeholder="Sampai Tanggal" required >
                                @if($errors->has("sampai"))
                                    <span class="help-block">{{ $errors->first("sampai") }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cetak</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });

        });

    </script>
@endsection