@if(count($pemesanan) > 0)
    @foreach($pemesanan as $data)
        <tr>
            <td>{{ (!empty($data->nama_pemesan) ? $data->nama_pemesan : "-") }}</td>
            <td>{{ $data->jadwal->tanggal }}</td>
            <td>{{ $data->jadwal->waktu }}</td>
            <td>{{ $data->jadwal->tempat }}</td>
            <td>{{ $data->total_peserta }}</td>
            <td>
                @if($data->order_status == "pending")
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary">Action</button>
                        <button type="button" class="btn btn-primary dropdown-toggle"
                                data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            
                            <li><a href="#" class="buat-pembayaran" data-id="{{ $data->id }}" data-total-bayar="{{ $data->pembayaran->payment_total }}">Buat Pembayaran</a>
                        </ul>
                    </div>
                @else
                    -
                @endif
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="6">Data Not Found</td>
    </tr>
@endif