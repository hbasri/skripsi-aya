<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Aloha!</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }

    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%"  >
    <tr>
        <td align="right">
            <h3>Laporan Pemesanan Table Manner</h3>
            <pre>
                Jalan Pedongkelan Belakang Rt.05/16 No.37B Kel. Kapuk Kec.Cengkareng, Jakarta Barat
                Telepon & Whatsapp : 089921342135
            </pre>
        </td>
    </tr>

  </table>

  <table width="100%">
    <tr>
        <td><strong>Dari :</strong> {{ $dari }}</td>
        <td><strong>Sampai :</strong> {{ $sampai }}</td>
    </tr>

  </table>

  <br/>

  <table width="100%" cellpadding="10">
    <thead style="background-color: lightgray;">
      <tr>
          <th>Nama Pemesanan</th>
          <th>Tanggal Private</th>
          <th>Waktu Private</th>
          <th>Tempat Private</th>
          <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
        @if(count($pemesanan) > 0)
    @foreach($pemesanan as $data)
        <tr>
            <td>{{ (!empty($data->nama_pemesan) ? $data->nama_pemesan : "-") }}</td>
            <td>{{ $data->jadwal->tanggal }}</td>
            <td>{{ $data->jadwal->waktu }}</td>
            <td>{{ $data->jadwal->tempat }}</td>
            <td>{{ $data->total_peserta }}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="5">Data Not Found</td>
    </tr>
@endif
      
    </tbody>
  </table>

</body>
</html>