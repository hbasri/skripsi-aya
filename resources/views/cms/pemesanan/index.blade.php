@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Pemesanan
            <small>Pemesanan Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Pemesanan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pemesanan</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="20%">Nama Pemesanan</th>
                                <th width="10%">Tanggal Private</th>
                                <th width="10%">Waktu Private</th>
                                <th width="10%">Tempat Private</th>
                                <th width="10%">Jumlah</th>
                                <th width="10%">Action</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchNama" class="form-control" placeholder="Nama Pemesan"></td>
                                <td><input type="text" name="searchTanggal" class="form-control datepicker" placeholder="Tanggal"> </td>
                                <td><input type="text" name="searchWaktu" class="form-control" placeholder="13:00:00"> </td>
                                <td><input type="text" name="searchTempat" class="form-control" placeholder="Tempat Private"> </td>
                                <td><input type="text" name="searchJumlah" class="form-control" placeholder="Jumlah"> </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="pemesanan-body">
                                @include("cms.pemesanan.pemesanan")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="pemesanan-paginate">
                        @include("cms.pemesanan.ul")
                    </div>
                </div>
                <!-- /.box -->
                
            </div>
            <!-- /.col -->
            <!-- Modal -->
            <div class="modal fade" id="pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Buat Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Nomor Rekening</label>
                            <input type="number" name="nomor_rekening" value="" class="form-control" placeholder="Nomor Rekening">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Tanggal Bayar</label>
                            <input type="text" name="tanggal_bayar" value="" class="form-control datepicker" placeholder="Tanggal Bayar">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Pembayaran Via</label>
                            <input type="text" name="bayar_via" value="" class="form-control" placeholder="Pembayaran Via">
                        </div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Total Pembayaran</label>
                            <input type="number" name="total_pembayaran" value="" class="form-control" placeholder="Total Pembayaran" readonly="true">
                        </div>
                    </div>

                    <input type="hidden" name="order_id">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" id="simpanPembayaran">Simpan</button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPemesanan(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getPemesanan($(this).attr('href').split('page=')[1]);
            });

            $(document).on('click', '.buat-pembayaran', function (e) {
                $("#pembayaran").find("input").val("");
                $("#pembayaran").find("input[name='total_pembayaran']").val($(this).attr("data-total-bayar"));
                $("#pembayaran").find("input[name='order_id']").val($(this).attr("data-id"));
                $("#pembayaran").modal();
            });

            $(document).on('click', '#simpanPembayaran', function (e) {
                var data = {
                    order_id : $("#pembayaran").find("input[name='order_id']").val(),
                    nomor_rekening : $("#pembayaran").find("input[name='nomor_rekening']").val(),
                    tanggal_bayar : $("#pembayaran").find("input[name='tanggal_bayar']").val(),
                    bayar_via : $("#pembayaran").find("input[name='bayar_via']").val()
                }

                $.post('{{ url("cms/pembayaran/create") }}', data, function(data, textStatus, xhr) {
                    if(data.status){
                        $("#pembayaran").modal("hide");
                        getPemesanan(1);
                    }
                });
            });
        });

        function getPemesanan(page) {
            $("#pemesanan-body").html("<tr align='center'><td colspan='6'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&nama=" + $("input[name='searchNama']").val() + "&tanggal=" + $("input[name='searchTanggal']").val() +"&waktu=" + $("input[name='searchWaktu']").val() + "&tempat=" + $("input[name='searchTempat']").val() + "&jumlah=" + $("input[name='searchJumlah']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#pemesanan-body').html(data.pemesanan);
                $("#pemesanan-paginate").html(data.ul_pemesanan);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[name='searchNama']").on("blur",function(){
                getPemesanan(1);
            });

            $("input[name='searchTempat']").on("blur",function(){
                getPemesanan(1);
            });

            $("input[name='searchWaktu']").on("blur",function(){
                getPemesanan(1);
            });

            $("input[name='searchTanggal']").datepicker().on('changeDate', function (ev) {
                getPemesanan(1);
            });

            $("input[name='searchJumlah']").on("blur",function(){
                getPemesanan(1);
            });
        });

    </script>
@endsection