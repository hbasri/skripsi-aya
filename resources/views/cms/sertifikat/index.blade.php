@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Sertifikat
            <small>Sertifikat Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Sertifikat</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Sertifikat</span>

                            <div class="margin">
                                <a href="{{ url("cms/sertifikat/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-users"></i>
                                        Sertifikat
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Sertifikat</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="25%">Nama Pemesan</th>
                                <th width="25%">Sertifikat</th>
                                <th width="12%">Action</th>
                            </tr>
                            <tr>
                                <td>
                                    <select name="searchOrderId" class="form-control">
                                        <option value="all">Semua</option>
                                        @foreach($pemesanan as $data)
                                            <option value="{{ $data->id }}">{{ $data->order_number }}</option>
                                        @endforeach
                                </td>
                               
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="sertifikat-body">
                                @include("cms.sertifikat.sertifikat")
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="sertifikat-paginate">
                        @include("cms.sertifikat.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getSertifikat(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getSertifikat($(this).attr('href').split('page=')[1]);
            });
        });

        function getSertifikat(page) {
            $("#sertifikat-body").html("<tr align='center'><td colspan='3'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&nama=" + $("input[name='searchNama']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#sertifikat-body').html(data.sertifikat);
                $("#sertifikat-paginate").html(data.ul_sertifikat);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[name='searchNama']").on("blur",function(){
               getSertifikat(1);
            });

        });

    </script>
@endsection