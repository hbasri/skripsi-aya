@if(count($sertifikat) > 0)
    @foreach($sertifikat as $data)
        <tr>
            <td>{{ (!empty($data->pemesanan->nama_pemesan) ? $data->pemesanan->nama_pemesan : "-") }}</td>
            <td>{!!  !empty($data->files) ? "<a href='".asset("assets/sertifikat/".$data->files)."'>Download</a>" : "-" !!}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("cms/sertifikat/edit/".$data->id) }}">Ubah</a>
                        </li>
                        <li><a href="{{ url("cms/sertifikat/".$data->id) }}"
                               data-method="delete" data-confirm="Delete this data?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="3">Data Not Found</td>
    </tr>
@endif