@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Sertifikat
            <small>Sertifikat Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Sertifikat</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Edit Sertifikat</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/sertifikat/'.$sertifikat->id) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="box-body">
                        
                            <div class="form-group {{ $errors->has("order_id") ? "has-error" : "" }}">
                                <label for="exampleInputEmail1">Genre</label>
                                <select name="order_id" class="form-control select2">
                                    <option disabled selected>Pilih</option>
                                    @foreach($pemesanan as $data)
                                        <option value="{{ $data->id }}" {{ $sertifikat->order_id == $data->id ? "selected" : "" }}>{{ $data->order_number }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has("order_id"))
                                    <span class="help-block">{{ $errors->first("order_id") }}</span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has("files") ? "has-error" : "" }}">
                                <label for="exampleInputFile">File</label>
                                <input type="file" name="files" class="form-control">

                                @if($errors->has("files"))
                                    <span class="help-block">{{ $errors->first("files") }}</span>
                                @endif
                            </div>
                            <input type="hidden" name="files_path" value="{{ $sertifikat->files }}">
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Cancel</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });

    </script>
@endsection