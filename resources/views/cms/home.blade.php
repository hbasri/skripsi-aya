@extends("layouts.cms")
@section("css_plugins")

@endsection
@section("content")
    <section class="content-header">
        <h1>
            Home
            <small>Dashboard Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <h3>Welcome {{ Auth::user()->name }}</h3>
    </section>
@endsection
@section("js_plugins")
    <!-- Morris.js charts -->
@endsection
