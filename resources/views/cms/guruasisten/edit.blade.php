@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("content")
    <section class="content-header">
        <h1>
            Guru dan Asisten
            <small>Guru dan Asisten Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Guru dan Asisten</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Ubah Guru dan Asisten</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('cms/guru-asisten/'.$guruasisten->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('nama') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" name="nama" value="{{ $guruasisten->nama }}" class="form-control" placeholder="Nama">
                                @if($errors->has("nama"))
                                    <span class="help-block">{{ $errors->first("nama") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('jenis_kelamin') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value="L" {{ $guruasisten->jenis_kelamin == "L" ? "selected" : "" }}>Laki-laki</option>
                                    <option value="P" {{ $guruasisten->jenis_kelamin == "P" ? "selected" : "" }}>Perempuan</option>
                                </select>
                                
                                @if($errors->has("jenis_kelamin"))
                                    <span class="help-block">{{ $errors->first("jenis_kelamin") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('kode_staff') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Kode Staff</label>
                                <input type="text" name="kode_staff" value="{{ $guruasisten->kode_staff }}" class="form-control" placeholder="Kode Staff">
                                @if($errors->has("kode_staff"))
                                    <span class="help-block">{{ $errors->first("kode_staff") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="text" name="email" value="{{ $guruasisten->email }}" class="form-control" placeholder="Email">
                                @if($errors->has("email"))
                                    <span class="help-block">{{ $errors->first("email") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('tipe_staff') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tipe Staff</label>
                                <select name="tipe_staff" class="form-control">
                                    <option value="guru" {{ $guruasisten->tipe_staff == "guru" ? "selected" : "" }}>Guru</option>
                                    <option value="asisten" {{ $guruasisten->tipe_staff == "asisten" ? "selected" : "" }}>Asisten</option>
                                </select>
                                
                                @if($errors->has("tipe_staff"))
                                    <span class="help-block">{{ $errors->first("tipe_staff") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('pendidikan_terakhir') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Pendidikan Terakhir</label>
                                <input type="text" name="pendidikan_terakhir" value="{{ $guruasisten->pendidikan_terakhir }}" class="form-control" placeholder="Pendidikan Terakhir">
                                @if($errors->has("pendidikan_terakhir"))
                                    <span class="help-block">{{ $errors->first("pendidikan_terakhir") }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('instansi_pendidikan_terakhir') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Instansi Pendidikan Terakhir</label>
                                <input type="text" name="instansi_pendidikan_terakhir" value="{{ $guruasisten->instansi_pendidikan_terakhir }}" class="form-control" placeholder="Instansi Pendidikan Terakhir">
                                @if($errors->has("instansi_pendidikan_terakhir"))
                                    <span class="help-block">{{ $errors->first("instansi_pendidikan_terakhir") }}</span>
                                @endif
                            </div>

                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{ url()->previous() }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

        });

    </script>
@endsection