@extends("layouts.cms")
@section("css_plugins")
    <link rel="stylesheet" href="{{ asset("assets/cms/plugins/datepicker/datepicker3.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/cms/select2/css/select2.css") }}">
@endsection
@section("css_custom")
    <style type="text/css">
        .button-action {
            float: left;
            margin-left: 15px;
        }

        .title-action {
            margin-left: 10px;
        }

        .search {
            /*margin-left: 400px;*/
            margin-top: 30px;
            float: right;
        }

        .search .form-control {
            width: 250px;
        }

        .search .btn-default {
            width: 80px;
        }

    </style>
@endsection
@section("content")

    <section class="content-header">
        <h1>
            Guru dan Asisten
            <small>Guru dan Asisten Data</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("cms/home") }}"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Guru dan Asisten</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                @endif
                <div class="box">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <div class="button-action">
                            <span class="title-action">Tambah Guru dan Asisten</span>

                            <div class="margin">
                                <a href="{{ url("cms/guru-asisten/create") }}">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-users"></i>
                                        Guru dan Asisten
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Guru dan Asisten</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10%">Nama</th>
                                <th width="10%">Jenis Kelamin</th>
                                <th width="10%">Kode Staff</th>
                                <th width="10%">Email</th>
                                <th width="10%">Tipe Staff</th>
                                <th width="10%">Pendidikan Terakhir</th>
                                <th width="10%">Instansi Pendidikan Terakhir</th>
                                <th width="10%">Action</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="searchNama" class="form-control" placeholder="Nama"></td>
                                <td><select name="searchJenisKelamin" class="form-control"><option value="all">Semua</option><option value="L">Laki-laki</option><option value="P">Perempuan</option></select></td>
                                <td><input type="text" name="searchKodeStaff" class="form-control" placeholder="Kode Staff"></td>
                                <td><input type="text" name="searchEmail" class="form-control" placeholder="Email"></td>
                                <td><select name="searchTipeStaff" class="form-control"><option value="all">Semua</option><option value="guru">Guru</option><option value="asisten">Asisten</option></select></td>
                                <td><input type="text" name="searchPendidikanTerakhir" class="form-control" placeholder="Pendidikan Terakhir"></td>
                                <td><input type="text" name="searchInstansiPendidikanTerakhir" class="form-control" placeholder="Instansi Pendidikan Terakhir"></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="guruasisten-body">
                                @include("cms.guruasisten.guruasisten")
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix" id="guruasisten-paginate">
                        @include("cms.guruasisten.ul")
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>

@endsection
@section("js_plugins")
    <script src="{{ asset("assets/cms/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/cms/select2/js/select2.js") }}"></script>
@endsection

@section("js_custom")
    <!-- page script -->
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getGuruAsisten(page);
                }
            }
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Pilih --"
            });

            $('.datepicker').datepicker({
                format: "yyyy/mm/dd"
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                getGuruAsisten($(this).attr('href').split('page=')[1]);
            });
        });

        function getGuruAsisten(page) {
            $("#guruasisten-body").html("<tr align='center'><td colspan='8'><img src='{{ asset('assets/cms/loading.gif') }}' height='50px' width='50px'></td></tr>");
            $.ajax({
                url: '?page=' + page + "&nama=" + $("input[name='searchNama']").val() + "&jenis_kelamin=" + $("select[name='searchJenisKelamin']").val() +"&kode_staff=" + $("input[name='searchKodeStaff']").val() + "&email="+$("input[name='searchEmail']").val() + "&tipe_staff="+$("select[name='searchJenisKelamin']").val()+"&pendidikan_terakhir=" +$("input[name='searchPendidikanTerakhir']").val() + "&instansi_pendidikan_terakhir="+ $("input[name='searchInstansiPendidikanTerakhir']").val(),
                dataType: 'json',
            }).done(function (data) {
                $('#guruasisten-body').html(data.guruasisten);
                $("#guruasisten-paginate").html(data.ul_guruasisten);
                location.hash = page;
            }).fail(function () {

            });
        }

        $(document).ready(function () {
            $("input[name='searchNama']").on("blur",function(){
                getGuruAsisten(1);
            });

            $("select[name='searchJenisKelamin']").on("change",function(){
                getGuruAsisten(1);
            });

            $("input[name='searchKodeStaff']").on("blur",function(){
                getGuruAsisten(1);
            });

            $("input[name='searchEmail']").on("blur",function(){
                getGuruAsisten(1);
            });

            $("select[name='searchTipeStaff']").on("change",function(){
                getGuruAsisten(1);
            });

            $("input[name='searchPendidikanTerakhir']").on("blur",function(){
                getGuruAsisten(1);
            });

            $("input[name='searchInstansiPendidikanTerakhir']").on("blur",function(){
                getGuruAsisten(1);
            });
        });

    </script>
@endsection