@if(count($guruasisten) > 0)
    @foreach($guruasisten as $data)
        <tr>
            <td>{{ !empty($data->nama) ? $data->nama : "-"  }}</td>
            <td>{{ !empty($data->jenis_kelamin) ? $data->jenis_kelamin : "-" }}</td>
            <td>{{ !empty($data->kode_staff) ? $data->kode_staff : "-" }}</td>
            <td>{{ !empty($data->email) ? $data->email : "-" }}</td>
            <td>{{ !empty($data->tipe_staff) ? $data->tipe_staff : "-" }}</td>
            <td>{{ !empty($data->pendidikan_terakhir) ? $data->pendidikan_terakhir : "-" }}</td>
            <td>{{ !empty($data->instansi_pendidikan_terakhir) ? $data->instansi_pendidikan_terakhir : "-" }}</td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Action</button>
                    <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("cms/guru-asisten/edit/".$data->id) }}">Ubah</a>
                        {{--</li>--}}
                        <li><a href="{{ url("cms/guru-asisten/".$data->id) }}"
                               data-method="delete" data-confirm="Hapus data ini?"
                               data-token="{{ csrf_token() }}">Hapus</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="8">Data Not Found</td>
    </tr>
@endif