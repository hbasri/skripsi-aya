<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset("assets/cms/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("assets/cms/font-awesome/css/font-awesome.min.css") }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset("assets/cms/ionicons/css/ionicons.min.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("assets/cms/dist/css/AdminLTE.min.css") }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset("assets/cms/dist/css/skins/_all-skins.min.css") }}">

    @yield("css_plugins")

    @yield("css_custom")
    <style type="text/css">
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;

        }

        input[type='number'] {
            -moz-appearance: textfield;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url("cms/home") }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">Table Manner</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Table Manner Admin</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->


                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset("assets/cms/images/user1.png") }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ asset("assets/cms/images/user1.png") }}" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->name }} - {{ ucwords(Auth::user()->level) }}
                                    <small>Login terakhir, {{ Auth::user()->last_login }}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url("cms/user/profile") }}"
                                       class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url("cms/logout") }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url("cms/logout") }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="{{ (Request::is("cms/home") ? "active" : "") }}"><a href="{{ url("cms/home") }}"><i
                                class="fa fa-dashboard"></i><span>Beranda</span></a>
                </li>

                <li class="{{ (Request::is("cms/daftar-harga") || Request::is("cms/daftar-harga/*") ? "active" : "") }}"><a href="{{ url("cms/daftar-harga") }}"><i
                                class="fa fa-money"></i><span>Daftar Harga</span></a>
                </li>

                <li class="{{ (Request::is("cms/pemesanan") || Request::is("cms/pemesanan/*") ? "active" : "") }}"><a href="{{ url("cms/pemesanan") }}"><i
                                class="fa fa-edit"></i><span>Pemesanan</span></a>
                </li>

                <li class="{{ (Request::is("cms/pembayaran") || Request::is("cms/pembayaran/*") ? "active" : "") }}"><a href="{{ url("cms/pembayaran") }}"><i
                                class="fa fa-calculator"></i><span>Pembayaran</span></a>
                </li>

                <li class="{{ (Request::is("cms/guru-asisten") || Request::is("cms/guru-asisten/*") ? "active" : "") }}"><a href="{{ url("cms/guru-asisten") }}"><i
                                class="fa fa-users"></i><span>Guru dan Asisten</span></a>
                </li>
                <li class="{{ (Request::is("cms/jadwal") || Request::is("cms/jadwal/*") ? "active" : "") }}"><a href="{{ url("cms/jadwal") }}"><i
                                class="fa fa-calendar"></i><span>Jadwal Guru Asisten</span></a>
                </li>
                <li class="{{ (Request::is("cms/sertifikat") || Request::is("cms/sertifikat/*") ? "active" : "") }}"><a href="{{ url("cms/sertifikat") }}"><i
                                class="fa fa-map-o"></i><span>Sertifikat</span></a>
                </li>
                <li class="{{ (Request::is("cms/users") || Request::is("cms/user/*") ? "active" : "") }}"><a href="{{ url("cms/users") }}"><i
                                class="fa fa-user"></i><span>Admin</span></a>
                </li>

                <li class="{{ (Request::is("cms/pemesanan/laporan") || Request::is("cms/pemesanan/laporan/*") ? "active" : "") }}"><a href="{{ url("cms/pemesanan/laporan") }}"><i
                                class="fa fa-external-link"></i><span>Laporan Pemesanan</span></a>
                </li>

                <li class="{{ (Request::is("cms/pembayaran/laporan") || Request::is("cms/pembayaran/laporan/*") ? "active" : "") }}"><a href="{{ url("cms/pembayaran/laporan") }}"><i
                                class="fa fa-external-link-square"></i><span>Laporan Pembayaran</span></a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @yield("content")

    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; {{ date("Y") }} Table Manner Admin.</strong> All rights
        reserved.
    </footer>
    <!-- Control Sidebar -->

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="{{ asset("assets/cms/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset("assets/cms/jquery-ui.min.js") }}"></script>
<script src="{{ asset("assets/cms/laravel.js") }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.6 -->
<script src="{{ asset("assets/cms/bootstrap/js/bootstrap.min.js") }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset("assets/cms/dist/js/app.min.js") }}"></script>
@yield("js_plugins")
<!-- AdminLTE for demo purposes -->
<script src="{{ asset("assets/cms/dist/js/demo.js") }}"></script>
<script src="{{ asset("assets/cms/jquery-number.min.js") }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield("js_custom")
</body>
</html>