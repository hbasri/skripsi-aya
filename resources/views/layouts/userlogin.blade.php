<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Warner Bros Review - {{ $title }}</title>

    <!-- faicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset("assets/images/faicon/apple-icon-57x57.png") }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset("assets/images/faicon/apple-icon-60x60.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset("assets/images/faicon/apple-icon-72x72.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset("assets/images/faicon/apple-icon-76x76.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset("assets/images/faicon/apple-icon-114x114.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset("assets/images/faicon/apple-icon-120x120.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset("assets/images/faicon/apple-icon-144x144.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset("assets/images/faicon/apple-icon-152x152.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/images/faicon/apple-icon-180x180.png") }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset("assets/images/faicon/android-icon-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("assets/images/faicon/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset("assets/images/faicon/favicon-96x96.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("assets/images/faicon/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ asset("assets/images/faicon/manifest.json") }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- /faicon -->

    <meta property="og:type" content="article" />
    <meta property="og:url" content="#" />
    <meta property="og:title" content="#" />
    <meta property="og:description" content="# " />
    <meta property="og:image" content="{{ asset("assets/images/thumb.jpg") }}" />
    <meta property="fb:admins" content="#" />
    <meta property="fb:app_id" content="#" />
    <meta name="twitter:card" content="#" />
    <meta name="twitter:site" content="#" />
    <meta name="twitter:creator" content="#">
    <meta name="twitter:title" content="#" />
    <meta name="twitter:description" content="#" />
    <meta name="twitter:image" content="{{ asset("assets/images/thumb.jpg") }}" />

    <!-- css -->
    <link rel="stylesheet" href="{{ asset("assets/css/styles.min.css") }}">

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,900" rel="stylesheet">
    @yield("css_script")

</head>
<body class="body body__review">
<div id="preloader">
    <div class="display-table">
        <div class="display-center">
            <div class="container">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page page__review">
    <div class="container">
        @yield("content")
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row">
            <p>© {{ date("Y") }} Warner Bros. Entertainment Inc. All rights reserved.</p>
        </div>
    </div>
</div>
<script src="{{ asset("assets/js/script.min.js") }}"></script>
<script src="{{ asset("assets/js/main.js") }}" charset="utf-8"></script>
<script src="{{ asset("assets/js/laravel.js") }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield("js_script")
</body>
</html>
