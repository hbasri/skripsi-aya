<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Table Manner Application</title>
    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- off-canvas -->
    <link href="{{ asset("assets/frontend/css/mobile-menu.css") }}" rel="stylesheet">
    <!-- font-awesome -->
    <link href="{{ asset("assets/fonts/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Style CSS -->
    <link href="{{ asset("assets/frontend/css/style.css") }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="main-wrapper">
<!-- Page Preloader -->
<div id="preloader">
    <div id="status">
        <div class="status-mes"></div>
    </div>
</div>

<div class="uc-mobile-menu-pusher">

<div class="content-wrapper">
<nav class="navbar m-menu navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt=""></a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="#navbar-collapse-1">

            <ul class="nav-cta hidden-xs">
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i
                        class="fa fa-search"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="head-search">
                                <form role="form">
                                    <!-- Input Group -->
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Type Something">
			                                <span class="input-group-btn">
			                                  <button type="submit" class="btn btn-primary">Search</button>
			                                </span>
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right main-nav">
                <li class="{{ Request::is("/") ? 'active' : '' }}"><a href="{{ url("/") }}">Beranda</a></li>
                @if(Auth::guard("customer")->check())
                    <li class="{{ Request::is("pemesanan") ? 'active' : '' }}"><a href="{{ url("pemesanan") }}">Pemesanan</a></li>
                    <li class="{{ Request::is("pembayaran") ? 'active' : '' }}"><a href="{{ url("pembayaran") }}">Status Pembayaran</a></li>
                    <li class="{{ Request::is("guru-asisten") ? 'active' : '' }}"><a href="{{ url("guru-asisten") }}">Guru & Asisten</a></li>
                    <li class="{{ Request::is("serfitikat") ? 'active' : '' }}"><a href="{{ url("sertifikat") }}">Sertifikat</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li><a href="{{ url("keluar") }}">Logout</a></li>
                @else
                    <li class="{{ Request::is("panduan") ? 'active' : '' }}"><a href="{{ url("panduan") }}">Panduan</a></li>
                    <li class="{{ Request::is("daftar-harga") ? 'active' : '' }}"><a href="{{ url("daftar-harga") }}">Daftar Harga</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li class="{{ Request::is("qa") ? 'active' : '' }}"><a href="{{ url("qa") }}">Q & A</a></li>
                    <li class="{{ Request::is("gallery") ? 'active' : '' }}"><a href="{{ url("gallery") }}">Gallery</a></li>
                    <li class="{{ Request::is("kontak") ? 'active' : '' }}"><a href="{{ url("kontak") }}">Kontak Kami</a></li>
                    <li class="{{ Request::is("masuk") ? 'active' : '' }}"><a href="{{ url("masuk") }}">Masuk</a></li>
                    <li class="{{ Request::is("daftar") ? 'active' : '' }}"><a href="{{ url("daftar") }}">Daftar</a></li>
                @endif
            </ul>

        </div>
        <!-- .navbar-collapse -->
    </div>
    <!-- .container -->
</nav>
<!-- .nav -->

@if(Request::is("/"))
    @include("front.slider")
@endif

@yield("content")

<footer class="footer">

    <!-- Footer Widget Section -->
    

    <div class="copyright-section">
        <div class="container clearfix">
                Table Manner Application V.1

            <ul class="list-inline pull-right">
                <li class="{{ Request::is("/") ? 'active' : '' }}"><a href="{{ url("/") }}">Beranda</a></li>
                @if(Auth::guard("customer")->check())
                    <li class="{{ Request::is("pemesanan") ? 'active' : '' }}"><a href="{{ url("pemesanan") }}">Pemesanan</a></li>
                    <li class="{{ Request::is("pembayaran") ? 'active' : '' }}"><a href="{{ url("pembayaran") }}">Status Pembayaran</a></li>
                    <li class="{{ Request::is("guru-asisten") ? 'active' : '' }}"><a href="{{ url("guru-asisten") }}">Guru & Asisten</a></li>
                    <li class="{{ Request::is("serfitikat") ? 'active' : '' }}"><a href="{{ url("serfitikat") }}">Sertifikat</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li><a href="{{ url("keluar") }}">Logout</a></li>
                @else
                    <li class="{{ Request::is("panduan") ? 'active' : '' }}"><a href="{{ url("panduan") }}">Panduan</a></li>
                    <li class="{{ Request::is("daftar-harga") ? 'active' : '' }}"><a href="{{ url("daftar-harga") }}">Daftar Harga</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li class="{{ Request::is("qa") ? 'active' : '' }}"><a href="{{ url("qa") }}">Q & A</a></li>
                    <li class="{{ Request::is("gallery") ? 'active' : '' }}"><a href="{{ url("gallery") }}">Gallery</a></li>
                    <li class="{{ Request::is("kontak") ? 'active' : '' }}"><a href="{{ url("kontak") }}">Kontak Kami</a></li>
                    <li class="{{ Request::is("masuk") ? 'active' : '' }}"><a href="{{ url("masuk") }}">Masuk</a></li>
                    <li class="{{ Request::is("daftar") ? 'active' : '' }}"><a href="{{ url("daftar") }}">Daftar</a></li>
                @endif
            </ul>
        </div><!-- .container -->
    </div><!-- .copyright-section -->
</footer>
<!-- .footer -->

</div>
<!-- .content-wrapper -->
</div>
<!-- .offcanvas-pusher -->

<div class="uc-mobile-menu uc-mobile-menu-effect">
    <button type="button" class="close" aria-hidden="true" data-toggle="offcanvas"
            id="uc-mobile-menu-close-btn">&times;</button>
    <div>
        <div>
            <ul id="menu">
                <li class="{{ Request::is("/") ? 'active' : '' }}"><a href="{{ url("/") }}">Beranda</a></li>
                @if(Auth::guard("customer")->check())
                    <li class="{{ Request::is("pemesanan") ? 'active' : '' }}"><a href="{{ url("pemesanan") }}">Pemesanan</a></li>
                    <li class="{{ Request::is("pembayaran") ? 'active' : '' }}"><a href="{{ url("pembayaran") }}">Status Pembayaran</a></li>
                    <li class="{{ Request::is("guru-asisten") ? 'active' : '' }}"><a href="{{ url("guru-asisten") }}">Guru & Asisten</a></li>
                    <li class="{{ Request::is("serfitikat") ? 'active' : '' }}"><a href="{{ url("serfitikat") }}">Sertifikat</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li><a href="{{ url("keluar") }}">Logout</a></li>
                @else
                    <li class="{{ Request::is("panduan") ? 'active' : '' }}"><a href="{{ url("panduan") }}">Panduan</a></li>
                    <li class="{{ Request::is("daftar-harga") ? 'active' : '' }}"><a href="{{ url("daftar-harga") }}">Daftar Harga</a></li>
                    <li class="{{ Request::is("review") ? 'active' : '' }}"><a href="{{ url("review") }}">Review</a></li>
                    <li class="{{ Request::is("qa") ? 'active' : '' }}"><a href="{{ url("qa") }}">Q & A</a></li>
                    <li class="{{ Request::is("gallery") ? 'active' : '' }}"><a href="{{ url("gallery") }}">Gallery</a></li>
                    <li class="{{ Request::is("kontak") ? 'active' : '' }}"><a href="{{ url("kontak") }}">Kontak Kami</a></li>
                    <li class="{{ Request::is("masuk") ? 'active' : '' }}"><a href="{{ url("masuk") }}">Masuk</a></li>
                    <li class="{{ Request::is("daftar") ? 'active' : '' }}"><a href="{{ url("daftar") }}">Daftar</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
<!-- .uc-mobile-menu -->

</div>
<!-- #main-wrapper -->


<!-- Script -->
<script src="{{ asset("assets/frontend/js/jquery-2.1.4.min.js") }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script src="{{ asset("assets/frontend/js/smoothscroll.js") }}"></script>
<script src="{{ asset("assets/frontend/js/mobile-menu.js") }}"></script>
<script src="{{ asset("assets/frontend/js/flexSlider/jquery.flexslider-min.js") }}"></script>
<script src="{{ asset("assets/frontend/js/scripts.js") }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield("js")
<div/>
	   Table Manner Application V.1        
	</div>
</body>
</html>