@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Kontak</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Kontak</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>contact</p>
            </div>
        </div>
    </div>

</section>

@endsection