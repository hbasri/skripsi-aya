@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Customer</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Form Tambah Customer</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{ url("daftar") }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Nama</label>
                                <input type="text" class="form-control" required="" placeholder="Nama" name="nama">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Email</label>
                                <input type="email" class="form-control" required="" placeholder="Email" name="email">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Telepon</label>
                                <input type="text" class="form-control" required="" placeholder="Telepon" name="telepon">
                            </div>
                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Alamat</label>
                                <input type="text" class="form-control" required="" placeholder="Alamat" name="alamat">
                            </div>

                            <div class="form-group">
                                <label for="emailFive" class="sr-only">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value="laki-laki">Laki-laki</option>
                                    <option value="perempuan">Perempuan</option>
                                </select>
                            
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Username</label>
                                <input type="text" class="form-control" required="" placeholder="Username" name="username">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Password</label>
                                <input type="password" class="form-control" required="" placeholder="Password" name="password">
                            </div>

                            <button type="submit" class="btn btn-primary">Daftar</button>
                           
                        </div><!-- /.col-md-6 -->
                        
                    </div><!-- /.row-->
                    
                </form>
            </div><!-- /.col-md-8 -->
        </div>
    </div>

</section>

@endsection