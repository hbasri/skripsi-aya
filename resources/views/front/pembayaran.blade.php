@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Pembayaran</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar List Pembayaran</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                	<tr>
                		<th>No</th>
                		<th>Nama Pemesan</th>
                		<th>Tanggal Bayar</th>
                		<th>Jumlah Bayar</th>
                        <th>Status Pembayaran</th>
                        <th>Cetak</th>
                	</tr>
                	@foreach($pembayaran as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                			<td>{{ $data->pemesanan->nama_pemesan }}</td>
                			<td>{{ (!empty($data->payment_date)) ? $data->payment_date : "-" }}</td>
                            <td>Rp {{ number_format($data->payment_total) }}</td>
                            <td>{{ $data->payment_status }}</td>
                            <td>{!! $data->payment_status == "lunas" ? "<a href='".url('invoice/'.$data->id)."' target='_blank'>Cetak</a>" : "-" !!}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection