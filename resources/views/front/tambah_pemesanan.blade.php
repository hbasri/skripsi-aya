@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Pemesanan</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Form Tambah Pemesanan</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{ url("pemesanan") }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Nama Pemesan</label>
                                <input type="text" class="form-control" required="" placeholder="Nama" name="nama_pemesan">
                            </div>

                            <div class="form-group">
                                <label for="nameFive-first" class="sr-only">Nama Paket</label>
                                <select name="daftar_harga_id" class="form-control" id="nama_paket">
                                    <option disabled selected>Pilih</option>
                                    @foreach($daftarharga as $data)
                                        <option value="{{ $data->id }}">{{ $data->daftarHarga->nama }}</option>
                                    @endforeach
                                </select>
                            
                            </div>

                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Tanggal Private</label>
                                <input type="text" class="form-control" required="" placeholder="Tanggal Private" name="tanggal_private" readonly="true">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Waktu Private</label>
                                <input type="text" class="form-control" required="" placeholder="Waktu Private" name="waktu_private" readonly="true">
                            </div>
                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Jumlah</label>
                                <input type="text" class="form-control" required="" placeholder="Jumlah" name="jumlah">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Tempat Private</label>
                                <input type="text" class="form-control" required="" placeholder="Tempat Private" name="tempat_private" readonly="true">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Jumlah Pembayaran</label>
                                <input type="text" class="form-control" required="" placeholder="Jumlah Bayar" name="jumlah_pembayaran" readonly="true">
                            </div>

                            <button type="submit" class="btn btn-primary">Pesan</button>
                           
                        </div><!-- /.col-md-6 -->
                        
                    </div><!-- /.row-->
                    
                </form>
            </div><!-- /.col-md-8 -->
        </div>
    </div>

</section>

@endsection

@section("js")
    <script>
        $("select[name='daftar_harga_id']").on("change",function(e){
           
            $.get("{{ url('daftarharga') }}/" + $(this).val(), function(data, status){
                $("input[name='tanggal_private']").val(data.paket.tanggal);
                $("input[name='waktu_private']").val(data.paket.waktu);
                $("input[name='tempat_private']").val(data.paket.tempat);
                $("input[name='jumlah_pembayaran']").val(data.paket.daftar_harga.harga);
            });
        });

        $("input[name='jumlah']").on("blur",function(e){
            if($(this).val() >= 10){
                $("input[name='tempat_private']").prop("readonly",false);
            }else{
                $("input[name='tempat_private']").prop("readonly",true);
            }
        })
    </script>
@endsection