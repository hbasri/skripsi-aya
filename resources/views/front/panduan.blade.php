@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Panduan</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Panduan</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Nama File</th>
                        <th>Download</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>ATURAN_DASAR_DALAM_ETIKA_MAKAN.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/ATURAN_DASAR_DALAM_ETIKA_MAKAN.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>ETIKA_ATAU_TATA_KRAMA_DALAM_MAKAN_DAN_MINUM.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/ETIKA_ATAU_TATA_KRAMA_DALAM_MAKAN_DAN_MINUM.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>HAL_YANG_HARUS_DIPERHATIKAN_DAN_YANG_HARUS_DIHINDARI_SAAT_JAMUAN_MAKAN_BERLANGSUNG.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/HAL_YANG_HARUS_DIPERHATIKAN_DAN_YANG_HARUS_DIHINDARI_SAAT_JAMUAN_MAKAN_BERLANGSUNG.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>MENGENAL_APA_ITU_TABLE_MANNER.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/MENGENAL_APA_ITU_TABLE_MANNER.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>MENGENAL_CARA_MEMPERGUNAKAN_PERALATAN_MAKANAN.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/MENGENAL_CARA_MEMPERGUNAKAN_PERALATAN_MAKANAN.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>MENGENAL_JENIS_PERALATAN_MAKANAN_DAN_CARA_MENGGUNAKANNYA.PDF</td>
                        <td><a href="{{ asset("assets/frontend/panduan/MENGENAL_JENIS_PERALATAN_MAKANAN_DAN_CARA_MENGGUNAKANNYA.pdf") }}" target="_blank">Download File</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</section>

@endsection