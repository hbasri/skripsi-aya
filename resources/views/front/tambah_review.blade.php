@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Review</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Tambah Review</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{ url("review/tambah") }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Nama</label>
                                <input type="text" class="form-control" required="" placeholder="Nama" name="nama">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Email</label>
                                <input type="email" class="form-control" required="" placeholder="Email" name="email">
                            </div>


                            <div class="form-group user-phone">
                                <label for="websiteOne" class="sr-only">Tanggal Private</label>
                                <input type="text" class="form-control" required=""  placeholder="Tanggal Private" name="tanggal_private">
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="form-group user-message">
                                <label for="messageOne" class="sr-only">Isi</label>
                                <textarea class="form-control" required="" placeholder="Isi Review" name="isi"></textarea>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </form>
            </div><!-- /.col-md-8 -->
        </div>
    </div>

</section>

@endsection