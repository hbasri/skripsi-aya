@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Guru dan Asisten</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar Guru, Asisten dan Jadwal</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                	<tr>
                		<th>No</th>
                		<th>Nama Pemesan</th>
                		<th>Jenis Kelamin r</th>
                		<th>Email</th>
                        <th>Pendidikan Terakhir</th>
                        <th>Instansi Pendidikan Terakhir</th>
                        <th>Status</th>
                	</tr>
                	@foreach($guruasisten as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                			<td>{{ $data->nama }}</td>
                			<td>{{ $data->jenis_kelamin }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->pendidikan_terakhir }}</td>
                            <td>{{ $data->instansi_pendidikan_terakhir }}</td>
                            <td>{{ $data->tipe_staff }}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>

        <br /><br /><br />

        <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Nama Guru</th>
                        <th>Nama Asisten</th>
                        <th>Tanggal Private</th>
                        <th>Waktu Private</th>
                        <th>Tempat Private</th>
                    </tr>
                    @foreach($jadwal as $key => $data)
                        <tr>
                            <td>{{ $key+=1 }}</td>
                            <td>{{ $data->guru->nama }}</td>
                            <td>{{ $data->asisten->nama }}</td>
                            <td>{{ $data->tanggal }}</td>
                            <td>{{ $data->tempat }}</td>
                            <td>{{ $data->waktu }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection