@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Pemesanan</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar List Pemesanan</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url("pemesanan/tambah") }}"><button type="button" class="btn btn-primary">Tambah Pemesanan</button></a>
                <br /><br/>
                <table class="table">
                	<tr>
                		<th>No</th>
                        <th>Nomor Pesanan</th>
                		<th>Nama Pemesan</th>
                		<th>Tanggal Private</th>
                		<th>Waktu Private</th>
                        <th>Jumlah</th>
                        <th>Tempat Private</th>
                	</tr>
                	@foreach($pemesanan as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                            <td>{{ $data->order_number }}</td>
                			<td>{{ $data->nama_pemesan }}</td>
                			<td>{{ $data->jadwal->tanggal }}</td>
                            <td>{{ $data->jadwal->waktu }}</td>
                            <td>{{ $data->total_peserta }}</td>
                            <td>{{ $data->tempat }}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection