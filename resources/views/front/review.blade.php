@extends("layouts.frontend")
@section("content")
<section class="single-page-title">
    <div class="container text-center">
        <h2>Review Customer</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar Review Customer</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( Auth::guard("customer")->check())
            	<a href="{{ url("review/tambah") }}"><button type="button" class="btn btn-primary">Tambah Review</button></a>
            	<br /><br/>
                @endif
                <table class="table">
                	<tr>
                		<th>No</th>
                		<th>Nama</th>
                		<th>Email</th>
                		<th>Tanggal Privat</th>
                		<th>Review</th>
                	</tr>
                	@foreach($reviews as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                			<td>{{ $data->nama }}</td>
                			<td>{{ $data->email }}</td>
                			<td>{{ $data->tanggal_private }}</td>
                			<td>{{ $data->isi }}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection