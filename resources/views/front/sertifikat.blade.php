@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Sertifikat</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar Sertifikat Peserta</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                	<tr>
                		<th>No</th>
                		<th>Nama Pemesan</th>
                		<th>Nomor Order</th>
                		<th>Sertifikat</th>
                	</tr>
                	@foreach($sertifikat as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                			<td>{{ $data->nama_pemesan }}</td>
                			<td>{{ $data->order_number }}</td>
                            <td>{!! !empty($data->files) ? "<a href='".asset("assets/sertifikat/".$data->files)."'>Download</a>" : "-" !!}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection