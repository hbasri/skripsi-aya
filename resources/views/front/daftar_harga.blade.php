@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Harga</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Daftar Harga Table Manner</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                	<tr>
                		<th>No</th>
                		<th>Nama</th>
                		<th>Deskripsi</th>
                		<th>Harga</th>
                	</tr>
                	@foreach($daftarHarga as $key => $data)
                		<tr>
                			<td>{{ $key+=1 }}</td>
                			<td>{{ $data->nama }}</td>
                			<td>{{ $data->deskripsi }}</td>
                			<td>Rp {{ number_format($data->harga,2) }}</td>
                		</tr>
                	@endforeach
                </table>
            </div>
        </div>
    </div>

</section>

@endsection