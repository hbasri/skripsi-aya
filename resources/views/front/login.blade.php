@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Login Customer</h2>
    </div>
</section>

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Form Login Customer</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{ url("masuk") }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Username</label>
                                <input type="text" class="form-control" required="" placeholder="Username" name="username">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Password</label>
                                <input type="password" class="form-control" required="" placeholder="Password" name="password">
                            </div>

                            <button type="submit" class="btn btn-primary">Masuk</button>
                           
                        </div><!-- /.col-md-6 -->
                        
                    </div><!-- /.row-->
                    
                </form>
            </div><!-- /.col-md-8 -->
        </div>
    </div>

</section>

@endsection