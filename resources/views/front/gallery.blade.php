@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Gallery</h2>
    </div>
</section>

<section class="service-icon-style ptb-100">
    <section class="section-title">
        <div class="container text-center">
            <h2>Gallery</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>


    <div class="container text-center">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/1.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/1.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/1-1.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/1-1.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/2.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/2.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/3.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/2-1.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
        
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/3-1.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/3-1.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/4.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/4.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/4-1.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/4-1.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/5.jpg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/5.jpg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                        <a href="{{ asset("assets/frontend/gallery/5.jpeg") }}" target="_blank"><img src="{{ asset("assets/frontend/gallery/5.jpeg") }}" width="100px" height="100px"></a>
                    </div>
                    
                </div>
                <!-- /.promo-block-wrapper -->
            </div>
        </div>
        <!-- /.row -->

    </div>


</section>

@endsection