@extends("layouts.frontend")
@section("content")

<section class="single-page-title">
    <div class="container text-center">
        <h2>Daftar Q & A</h2>
    </div>
</section>

<section class="ptb-100 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 x-accordion">
                <h3>Q & A</h3>
                <div class="panel-group" id="accordionSixLeft">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordionSixLeft" href="#collapseSixLeftone" aria-expanded="false" class="collapsed">
                                    Seamlessly empower
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSixLeftone" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs cond mentum leo massa mollis estiegittis miristum nulla.
                            </div>
                        </div>
                    </div><!-- /.panel-default -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionSixLeft" href="#collapseSixLeftTwo" aria-expanded="false">
                                    Initiatives.
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSixLeftTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs cond mentum leo massa mollis estiegittis miristum nulla.
                            </div>
                        </div>
                    </div><!-- /.panel-default -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="" data-toggle="collapse" data-parent="#accordionSixLeft" href="#collapseSixLeftThree" aria-expanded="true">
                                    Objectively maintain
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSixLeftThree" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs cond mentum leo massa mollis estiegittis miristum nulla.
                            </div>
                        </div>
                    </div><!-- /.panel-default -->
                </div><!--end of /.panel-group-->
            </div>
            <!-- .x-aacordion-->

        </div>
        <!-- /.row -->

    </div>


</section>
@endsection